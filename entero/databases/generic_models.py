from sqlalchemy import Integer, Column, ForeignKey, Table, String, DateTime, Time, Text, MetaData, func, Float,BigInteger
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base,declared_attr
from sqlalchemy.orm import relationship, backref, deferred
import random, string
import datetime
import json
import os
from sqlalchemy.schema import Sequence
from sqlalchemy.dialects.postgresql import JSONB, JSON
from requests.exceptions import ReadTimeout, ConnectTimeout
import requests

db_codes = {'senterica' : ['salmonella','SAL'],
            'ecoli' : ['escherichia','ESC'],
            'mcatarrhalis' : ['morexella','MOR'],
            'yersinia': ['yersinia','YER'],
            'listeria': ['listeria','LIS'],
            'neisseria': ['neisseria','NEI'],
            'mycobacterium': ['mycobacterium','MYC'],
            'miu':['miu',"MIU"]}

table_codes = dict(strains='SS', datadefs='DE',schemes='SC',
                   assemblies='AS', alelles='AL',sts='ST',
                   refsets='RE',traces='TR',loci='LO')


def new_unique_id(context):
    salt =  ''.join(random.choice(string.ascii_letters) for i in range(6))
    if context:
        table = context.statement.split()[2]
        rowcount = context.rowcount
    else:
        table = 'unknown'
        rowcount = 9000
    new_id =  str(table) + '-'  + str(rowcount + 1) + salt
    return new_id


#the next method use an attribute (database) which does not have a reference
# I will comment it for the ime being as I think it is not in use
'''
def encode(input_number,table_name,int_val=10000):
    number = input_number / int_val
    final_char = ''
    while number > 0:
        final_char += chr(number % 26 + 65)
        number =  (number / 26) 
    final_char= final_char.ljust(4,'A')#
    table_id = '' 
    if not table_name == 'strains':  table_id = '_%s' %table_codes[table_name]
    return  db_codes[database][1] + '_' + final_char[:2] + str(input_number % int_val).zfill(4) + final_char[2:] + table_id
'''
def decode(barcode,int_val=10000):
    prefix = barcode.split('_')[1][:2]
    suffix = barcode.split('_')[1][6:]
    char_string = (prefix+suffix).rstrip('A')
    int_string = 0
    for idx, cha in enumerate(char_string):
        int_string += 26 ** idx  * ((ord(cha) - 65))
    int_string = int_string * int_val
    return int_string + int(barcode[6:10])


def getSTAssemblyTable(metadata):
    return Table('st_assembly', metadata,
                 Column("assembly_id", Integer, ForeignKey('assemblies.id',ondelete="CASCADE")),
                 Column("st_id",Integer,ForeignKey('sts.id',ondelete="CASCADE")))

def getTraceAssemblyTable(metadata):
    return Table('trace_assembly', metadata,
                 Column("trace_id", Integer, ForeignKey('traces.id',ondelete="CASCADE")),
                 Column("assembly_id",Integer,ForeignKey('assemblies.id',ondelete="CASCADE")))

def getSTAlleleTable(metadata):
    return Table('st_allele', metadata,
                 Column("st_id",Integer,ForeignKey('sts.id',ondelete="CASCADE")),
                 Column("allele_id", Integer, ForeignKey('alleles.id',ondelete="CASCADE")))

#def getSchemeLociTable(metadata):
    #return Table('scheme_loci', metadata,
                 #Column("scheme_id",Integer,ForeignKey('schemes.id',ondelete="CASCADE")),
                 #Column("loci_id", Integer, ForeignKey('loci.id',ondelete="CASCADE")))

#def getAlleleAssemblyTable(metadata):
    #return Table('allele_assembly', metadata,
                 #Column("allele_id",Integer,ForeignKey('alleles.id',ondelete="CASCADE")),
                 #Column("assembly_id", Integer, ForeignKey('assemblies.id',ondelete="CASCADE")),
                 #Column("refstrain", Integer),
                 #Column("contig", String),
                 #Column("start", Integer),
                 #Column("stop", Integer),
                 #Column("direction",String(1)))


### Raw data tables ###    
class Assemblies(object):
    __tablename__ = 'assemblies'
    __mapper_args__= {'always_refresh': True}
    id = Column("id",Integer, primary_key=True, autoincrement=True)
    file_pointer = Column("file_pointer",String)
    user_id = Column("user_id",Integer)
    n50 = Column("n50",Integer)
    contig_number=Column("contig_number",Integer)
    low_qualities=Column("low_qualities",Integer)
    total_length =Column("total_length",Integer)
    top_species= Column("top_species",String)
    date_uploaded= Column("date_uploaded",DateTime, default=datetime.datetime.utcnow())
    file_name= Column("file_name",String(60))
    status= Column("status",String(60), index=True)
    # DB Metadata
    created =  Column("created",DateTime, default=datetime.datetime.utcnow())
    lastmodified =  Column("lastmodified",DateTime, default=datetime.datetime.utcnow(), onupdate=datetime.datetime.utcnow())
    lastmodifiedby =  Column("lastmodifiedby",Integer)
    index_id =  Column("index_id", String, default=new_unique_id, index=True)
    barcode =  Column("barcode", String, index=True, unique=True)
    version = Column('version',Integer,default=1)
    pipeline_version=Column('pipeline_version',Float)
    coverage=Column('coverage',Float)
    job_id=Column("job_id",Integer)
    other_data=Column('other_data',String)
    
    def add_other_data(self,putDic):
        if self.other_data:
            dic = json.loads(self.other_data)
            for key in putDic:
                dic[key]=putDic[key]
            
            self.other_data=json.dumps(dic)
        else:
            self.other_data=json.dumps(putDic)
            
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class AssembliesRel(Assemblies):
    # Relationships
    
    @declared_attr
    def nserv_sts(cls):    
        return relationship("AssemblyLookup", backref='assembly_list',cascade="delete",lazy='joined')
    
    @declared_attr 
    def strain_meta(cls):
        return relationship("Strains", backref='assembly',lazy='joined')
    
    @declared_attr
    def traces(cls):
        return relationship("Traces", secondary="trace_assembly", backref='assemblylist')

class AssembliesArchive(Assemblies):
    __tablename__ = 'assemblies_archive'
    index_id =  Column("index_id", String, default=new_unique_id, index=True)
    barcode =  Column("barcode", String, index=True)
    
class Traces(object):
    __tablename__ = 'traces'
    __mapper_args__= {'always_refresh': True}
    id = Column("id",Integer, primary_key=True, autoincrement=True)
    @declared_attr
    def strain_id(cls):
        return Column("strain_id",Integer,
                       ForeignKey('strains.id',ondelete="CASCADE"),  index=True)
    status = Column("status",String(200))

    seq_platform = Column("seq_platform",String(200))
    seq_library = Column("seq_library",String(200))
    seq_insert = Column("seq_insert",Integer)
    seq_version = Column("seq_version",Integer, default=1)
    total_bases = Column("total_bases",BigInteger)
    average_length = Column("average_length",Integer)
    
    experiment_accession = Column("experiment_accession",String(20))
    accession = Column("accession",String(100), index=True, default=new_unique_id) #is read accession
    read_location = Column("read_location",String(200))
    traces_comment = Column("traces_comment",Text)

    # DB Metadata
    created =  Column("created",DateTime, default=datetime.datetime.utcnow())
    lastmodified =  Column("lastmodified",DateTime, default=datetime.datetime.utcnow(), onupdate=datetime.datetime.utcnow())
    lastmodifiedby =  Column("lastmodifiedby",Integer)
    index_id =  Column("index_id", String, default=new_unique_id, index=True)
    barcode =  Column("barcode", String, index=True)
    version = Column('version',Integer,default=1)
    
    #returns a comma delimited list of read names (if user input data) or 
    #accession number associated with this trace
    def get_read_names(self):
        read_names =  "SRA:"+self.accession
        if self.accession.startswith("traces"):
            n_paths=[]
            l_paths = self.read_location.split(",")
            for path in l_paths:
                n_paths.append(os.path.basename(path))
            read_names= "Reads:"+",".join(n_paths)   
        return read_names
    
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class TracesArchive(Traces):
    __tablename__ = 'traces_archive'
    index_id =  Column("index_id", String, default=new_unique_id, index=True)
    barcode =  Column("barcode", String, index=True)

class Schemes(object):
    '''Defines a scheme, mlst, rmlst'''
    __tablename__ = 'schemes'
    __mapper_args__= {'always_refresh': True}
    id = Column("id",Integer, Sequence(__tablename__ + '_id_seq'), primary_key=True, index=True)
    description = Column("description",Text, index=True, unique=True)
    created =  Column("created",DateTime, default=datetime.datetime.utcnow())
    # Relationships
#    loci = relationship(Loci,backref='schemelist')
#    sts = relationship(SequenceTypes,backref='schemelist')
    # DB Metadata
    version = Column('version',Integer,default=1)
    created =  Column("created",DateTime, default=datetime.datetime.utcnow())
    lastmodified =  Column("lastmodified",DateTime, default=datetime.datetime.utcnow(), onupdate=datetime.datetime.utcnow())
    lastmodifiedby =  Column("lastmodifiedby",Integer)
#    index_id =  Column("index_id", String, default=new_unique_id, index=True)
#    default = Column("default",Integer)
#    st_complex_label = Column("st_complex_label",String)
#   lineage_label = Column("lineage_label",String)
#   subspecies_label = Column("subspecies_label",String)
#    js_grid  = Column("js_grid",String(20))
    name = Column("name",String(50))
#    pipeline_scheme=Column("pipeline_scheme",String(50))
#   default=Column("default",Integer)
    barcode =  Column("barcode", String, index=True)
    param = Column("param",JSONB)
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
    
    def get_param(self, param_name):
        return self.param.get(param_name)
    
class SchemesArchive(Schemes):
    __tablename__ = 'schemes_archive'
    index_id =  Column("index_id", String, default=new_unique_id, index=True)
    barcode =  Column("barcode", String, index=True)
    

class Strains(object):
    '''Isolate, sample, strain information'''
    __tablename__ = 'strains'
    __mapper_args__= {'always_refresh': True}
    id = Column("id",Integer, Sequence(__tablename__ + '_id_seq'), primary_key=True, index=True)
    comment = Column("comment",Text)
    strain = Column("strain",String(200))

    contact = Column("contact",String)
    source_niche = Column("source_niche",String)
    source_type = Column("source_type",String)
    source_details = Column("source_details",String(200))
    collection_year =  Column("collection_year", Integer)
    collection_month =  Column("collection_month", Integer)
    collection_date =  Column("collection_date", Integer)
    collection_time =  Column("collection_time", Time)
    continent = Column("continent",String(200))
    country = Column("country",String(200))
    admin1 = Column("admin1",String)
    admin2 = Column("admin2",String)
    city = Column("city",String(200))
    postcode = Column("postcode",String(200))
    owner = Column("owner",Integer)
    latitude = Column("latitude",Float)
    longitude = Column("longitude",Float)
    study_accession=Column("study_accession",String(20))
    secondary_study_accession =Column("secondary_study_accession",String(20))
    sample_accession = Column("sample_accession", String(20))
    secondary_sample_accession = Column("secondary_sample_accession",String(20))
    email = Column("email",String(50))
    @declared_attr
    def best_assembly(cls):
        return Column("best_assembly", Integer,
                       ForeignKey('assemblies.id'))        
    verified = Column("verified",Integer,default=0)
    # Relationships
    #   genomelist = relationship(Genomes, backref='strainlist')
    # DB Metadata
    version = Column('version',Integer,default=1)
    created =  Column("created",DateTime, default=datetime.datetime.utcnow())
    lastmodified =  Column("lastmodified",DateTime, default=datetime.datetime.utcnow(), onupdate=datetime.datetime.utcnow())
    lastmodifiedby =  Column("lastmodifiedby",Integer)
    release_date = Column("release_date",DateTime)
    index_id =  Column("index_id", String, default=new_unique_id, index=True)
    barcode =  Column("barcode", String, index=True,unique=True)
    antibiotic_resistance = Column("antibiotic_resistance",String)
    uberstrain =  Column("uberstrain", Integer, index = True)
    custom_columns =  Column("custom_columns", JSONB, index = True)
    
    
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
    
        
class StrainsArchive(Strains):
    __tablename__ = 'strains_archive'
    __mapper_args__= {'always_refresh': True}

    id = Column("id",Integer, Sequence(__tablename__ + '_id_seq'), primary_key=True, index=True)
    index_id =  Column("index_id", String, default=new_unique_id, index=True)
    barcode =  Column("barcode", String, index=True)
    uberstrain =  Column("uberstrain", Integer, index = True)


class DataParam(object):
    __tablename__ = 'data_param'
    __mapper_args__= {'always_refresh': True}
    id = Column("id",Integer, Sequence(__tablename__ + '_id_seq'), primary_key=True, index=True)
    tabname = Column("tabname",String)
    name= Column("name",String)
    sra_field = Column("sra_field",String)
    mlst_field = Column("mlst_field",String)
    display_order = Column("display_order", Integer)
    nested_order = Column("nested_order", Integer)
    label = Column("label", String)
    datatype = Column("datatype", String)
    min_value = Column("min_value", Integer)
    max_value = Column("max_value", Integer)
    required = Column("required", Integer)
    default_value = Column("default_value", String)
    allow_pattern = Column("allow_pattern", String)
    description = Column("description", Text)
    vals = Column("vals", String)
    max_length = Column("max_length", Integer)
    no_duplicates = Column("no_duplicates", Integer)
    allow_multiple_values = Column("allow_multiple_values", Integer)
    group_name = Column("group_name", String)    
    param = Column(JSONB)    

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class AssemblyLookup(object):
    __tablename__ = 'assembly_lookup'
    __mapper_args__= {'always_refresh': True}
    
    id = Column("id",Integer, Sequence(__tablename__ + '_id_seq'), primary_key=True, index=True)
    @declared_attr
    def assembly_id(cls):
        return Column("assembly_id", Integer,
                       ForeignKey('assemblies.id',ondelete="CASCADE"), index=True)    
    st_barcode =Column("st_barcode",String, index=True)
    @declared_attr    
    def annotation(cls):
        return deferred(Column("annotation",Text))
    @declared_attr
    def scheme_id(cls):
        return Column("scheme_id", Integer,
                       ForeignKey('schemes.id'), index=True)
    status=Column("status",String)
    version=Column("version",String,default='0.0')
    other_data=Column("other_data",JSON)
    
    def update_st_cache(self,  dat ): 
        # Given a dict of results from Nserv, add these to this assembly_lookup record. 
        # return ST id 
        altered = False
        if not self.other_data:
            self.other_data = {} 
        if not self.other_data.get('results'):
            self.other_data['results']  = {} 
            altered = True        
        if not self.other_data['results'].get('timestamp'):
            print 'no timestamp'
        
        if not dat:
            return None,altered
        if dat.get('ST_id'): 
            if  dat.get('info'):
                info = dat.get('info')
                results = self.other_data['results']
                results.pop('predicted_serotype', None)
                if  info.get('predict'):
                    if  info.get('predict').get('serotype'):
                        top = info.get('predict').get('serotype')[0][1]
                        total = 0
                        for sero in info.get('predict').get('serotype'):
                            total += sero[1]
                        if float(top) / float(total) > 0.75 :
                            if results.get('predicted_serotype') != info.get('predict').get('serotype')[0][0]:
                                results['predicted_serotype'] = info.get('predict').get('serotype')[0][0]
                                altered = True
                        else:
                            results.pop('predicted_serotype', None)
                            altered = True
                if info.get('lineage'):
                    if info.get('lineage') != 'None': 
                        if results.get('lineage') != info.get('lineage'):
                            results['lineage'] = info.get('lineage')
                            altered = True
                if info.get('st_complex'):
                    if info.get('st_complex') != 'None': 
                        if results.get('st_complex') != info.get('st_complex'):
                            results['st_complex'] = info.get('st_complex')
                            altered = True
                if info.get('subspecies'):
                    if info.get('subspecies') != 'None': 
                        if results.get('subspecies') != info.get('subspecies'):
                            results['subspecies'] = info.get('subspecies')
                            altered = True
                if info.get('genus'):
                    if info.get('genus') != 'None': 
                        if info.get('genus') != results.get('genus'):
                            results['genus'] = info.get('genus')
                            altered = True
                if info.get('species'):
                    
                    if info.get('species') != 'None': 
                        if results.get('species') != info.get('species'):
                            results['species'] = info.get('species')        
                            altered = True
            if self.other_data['results'].get('st_id') != dat.get('ST_id'):
                self.other_data['results']['st_id'] = dat.get('ST_id')
                altered = True
            if altered:
                self.other_data['results']['timestamp'] = unicode(datetime.datetime.now())
            return dat.get('ST_id'), altered
        return None, altered
    
    
    
    def get_st_number(self, SERVER,nserv_db_name,nserv_scheme, lowertime=1, uppertime=10):#
        # Returrn ST_id from cache and whether the value has expired and needs updating. 
        if not self.st_barcode or self.st_barcode == 'EGG_ST':
            return None, False
        # Check expiry date    
        if isinstance(self.other_data,unicode) or isinstance(self.other_data,str):
            self.other_data = json.dumps(self.other_data)
        if isinstance(self.other_data,unicode) or isinstance(self.other_data,str) or self.other_data is None :
            self.other_data = {} 
        if self.other_data.get('results'):
            time = self.other_data.get('results').get('timestamp')
            if time: 
                created = datetime.datetime.strptime(time, '%Y-%m-%d %H:%M:%S.%f') 
                if created  > (datetime.datetime.now() - datetime.timedelta(minutes=random.randint(lowertime, uppertime))):
                    return self.other_data.get('results').get('st_id'), False
                else: 
                    return self.other_data.get('results').get('st_id'), True 
        return None, True
                
    
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}    
    

    

