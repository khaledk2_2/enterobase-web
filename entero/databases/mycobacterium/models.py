from sqlalchemy import Integer, Column, ForeignKey, Table, String, DateTime, Time, Text, MetaData
from sqlalchemy.ext.declarative import declarative_base
from entero.databases import generic_models as mod

''''
metadata = MetaData()
Base = declarative_base(metadata=metadata)

### Lookup tables ###
# N.B Table order matters! defined relationships() in raw data tables
# need lookup, index, and dependent child tables to exist first.
STAssembly = mod.getSTAssemblyTable(metadata)
STAllele = mod.getSTAlleleTable(metadata)
SchemeLoci= mod.getSchemeLociTable(metadata)
AlleleAssembly = mod.getAlleleAssemblyTable(metadata)
TracesAssembly = mod.getTraceAssemblyTable(metadata)

### Index tables ###
class CitationsIndex(Base,mod.CitationsIndex):
    pass

class Meta(Base,mod.Meta):
    pass

class TableIndex(Base,mod.TableIndex):
    pass

### Raw data tables ###
class SequenceTypes(Base,mod.SequenceTypesRel):
    pass

class SequenceTypesArchive(Base, mod.SequenceTypesArchive):
    pass

class Assemblies(Base, mod.AssembliesRel):
    pass

class AssembliesArchive(Base, mod.AssembliesArchive):
    pass

class Traces(Base, mod.Traces):
    pass

class TracesArchive(Base, mod.TracesArchive):
    pass


class Alleles(Base,mod.AllelesRel):
    pass

class AllelesArchive(Base,mod.AllelesArchive):
    pass

class Schemes(Base,mod.Schemes):
    pass

class SchemesArchive(Base,mod.SchemesArchive):
    pass

class Loci(Base,mod.LociRel):
    pass

class LociArchive(Base,mod.LociArchive):
    pass

class Citations(Base,mod.Citations):
    pass

class Strains(Base,mod.Strains): 
    species = Column("species",String(200))
    serotype = Column("serotype",String(50))
    disease = Column("disease",String(200))
    path_nonpath = Column("path_nonpath",String(200))
    simple_pathogenesis = Column("simple_pathogenesis",String(200))
    simple_disease = Column("simple_disease",String(200))
    serological_group = Column("serological_group",String(200))
    contact_name=Column("contact_name",String)
    
       
class StrainsArchive(Base,mod.StrainsArchive):
    species = Column("species",String(200))
    serotype = Column("serotype",String(50))
    disease = Column("disease",String(200))
    path_nonpath = Column("path_nonpath",String(200))
    simple_pathogenesis = Column("simple_pathogenesis",String(200))
    simple_disease = Column("simple_disease",String(200))
    serological_group = Column("serological_group",String(200))
    contact_name=Column("contact_name",String)
    

class Tasks(Base,mod.Tasks): 
    pass 
class DataParam(Base,mod.DataParam): 
    pass
class UserFields(Base,mod.UserFields):
    pass






class AssemblyLookup(Base,mod.AssemblyLookup):
    pass
'''
metadata = MetaData()
Base = declarative_base(metadata=metadata)

### Lookup tables ###
# N.B Table order matters! defined relationships() in raw data tables
# need lookup, index, and dependent child tables to exist first.
#STAssembly = mod.getSTAssemblyTable(metadata)
#STAllele = mod.getSTAlleleTable(metadata)
# SchemeLoci= mod.getSchemeLociTable(metadata)
# AlleleAssembly = mod.getAlleleAssemblyTable(metadata)
TracesAssembly = mod.getTraceAssemblyTable(metadata)


### Raw data tables ###
class Assemblies(Base, mod.AssembliesRel):
    pass


class AssembliesArchive(Base, mod.AssembliesArchive):
    pass


class Traces(Base, mod.Traces):
    pass


class TracesArchive(Base, mod.TracesArchive):
    pass


class Schemes(Base, mod.Schemes):
    pass


class SchemesArchive(Base, mod.SchemesArchive):
    pass


class Strains(Base, mod.Strains):
    species = Column("species", String(200))
    serotype = Column("serotype", String(50))
    disease = Column("disease", String(200))
    path_nonpath = Column("path_nonpath", String(200))
    simple_pathogenesis = Column("simple_pathogenesis", String(200))
    simple_disease = Column("simple_disease", String(200))
    serological_group = Column("serological_group", String(200))

class StrainsArchive(Base, mod.StrainsArchive):
    species = Column("species", String(200))
    serotype = Column("serotype", String(50))
    disease = Column("disease", String(200))
    path_nonpath = Column("path_nonpath", String(200))
    simple_pathogenesis = Column("simple_pathogenesis", String(200))
    simple_disease = Column("simple_disease", String(200))
    serological_group = Column("serological_group", String(200))


class DataParam(Base, mod.DataParam):
    pass


class AssemblyLookup(Base, mod.AssemblyLookup):
    pass






