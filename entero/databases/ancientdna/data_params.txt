tabname	name	sra_field	display_order	nested_order	label	datatype	min_value	max_value	required	default_value	allow_pattern	description	vals	max_length	no_duplicates	allow_multiple_values	group_name
strains	strain				Sample	text											
strains	category				Category of Record	text											
strains	citation				Citation	text											
strains	disease				Disease	text											
strains	host_ethinity				Host Ethinity	text											
strains	host_sex				Host Sex	text											
strains	host_age				Host Age	text											
strains	altern_name				Altern Name	text											
strains	detailed_address				Detailed Address	text											
strains	year_range				Year Range	text											
strains	year_bp				Median Year (BP)	integer											
