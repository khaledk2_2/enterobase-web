/* global SRAValidationGrid */

UberStrainGrid.prototype = Object.create(SRAValidationGrid.prototype);
UberStrainGrid.prototype.constructor= UberStrainGrid;

function UberStrainGrid(name,config,species){
     //dictionary of uber strain id to a dictionary containing
     //status: open or closed
     //substrains: list of sub strain ids or undefined if no substrains
     this.uberStrain={};
     //call super constructor
     SRAValidationGrid.call(this,name,config,species);
     //callbacks may be called in different context therere can't use this for the grid
     //store a reference to this in self
     var self = this;
     //call back function given the display cell,the value to display and the rowID
     this.addExtraRenderer("uberstrain",function(cell,value,rowID,colName,rowIndex){
                var uber = self.uberStrain[rowID]
                var c=$(cell);
                if (uber){
                        var substrains = uber.substrains;
                        var state = uber.state;
                        //default symbol for uber strain with no substrains
                        var icon = 'glyphicon glyphicon-stop'
                        //put the open or closed symbol
                        if (substrains){
                                if (state==='closed'){
                                       icon = 'glyphicon glyphicon-triangle-right'
                                }
                                else{
                                        icon= 'glyphicon glyphicon-triangle-bottom'
                                }
                        }
                        var uberCol = self.getColumnIndex("barcode");

                        c.html($("<span class='"+icon+"' style='font-size:+"+self.fontSize+"'></span>"));
                        //if open or close change cursor to pointer
                        if (substrains){
                                c.css('cursor','pointer')
                        }
                        //this.encodeBarcode(value, this.data[0].values['barcode'].substring(0,3) ) 
                        c.append('<b>' + self.getValueAt(rowIndex,uberCol) + '</b>');

                }else{
                        var nextID = self.getRowId(rowIndex+1);
                        
                        var uber_d = self.uberStrain[nextID]
                        if (uber_d) {
                                c.html("&nbsp;&nbsp;&nbsp;&boxur;");
                        }else{
                                c.html("&nbsp;&nbsp;&nbsp;&boxvr;");

                        }                       
                }
        
        });
        
        this.addCustomColumnHandler("uberstrain",function(cell,rowIndex,rowID){
                var state = self.uberStrain[rowID].state;
                if (self.uberStrain[rowID].substrains){
                        if (state==='closed'){
                                self.uberStrain[rowID].state='open';
                        }
                        else {
                                self.uberStrain[rowID].state='closed';
                        }
                        self.refreshGrid();
                }
         });
         this.setCustomFileParser("uberstrain",function(rowIndex,colIndex){
              
              var colIndex = self.getColumnIndex("uberstrain");
              var uber = self.getValueAt(rowIndex,colIndex);
              var data = self.IDIndex[uber];
              return data['columns'][self.getColumnIndex('barcode')];            
        },
        function(text,columnName,rowID,headers,values){       
                return self.decodeBarcode(text);
        });


};


/**
* Expands all Uberstrains such that all substrains will be visible in the table
*/
UberStrainGrid.prototype.expandAllSubstrains= function(){
        for (var rowID in this.uberStrain){
                var uber = this.uberStrain[rowID];
                if (uber){
                        uber.state="open"            
                }
        }
        this.refreshGrid();

}

/**
* Closes all Uberstrains such that no substrains will be visible in the table
*/
UberStrainGrid.prototype.closeAllSubstrains= function(){
        for (var rowID in this.uberStrain){
                var uber = this.uberStrain[rowID];
                if (uber){
                        uber.state="closed"            
                }
        }
        this.refreshGrid();

}




/**
* Cleans up the ubertrain 
*/
UberStrainGrid.prototype.resetGrid = function(){
        this.uberStrain={};
        SRAValidationGrid.prototype.resetGrid.call(this);
}


/**
* Given a number, this method returns the appropriate barcode
* @param {integer} input_number - The id of the strain
* @param {string} db_code -The three letter code representing the database e.g SAL
* @returns {string} The strain barcode
*/
UberStrainGrid.prototype.encodeBarcode = function(input_number, db_code){
        var int_val = 10000;
        var number = parseInt(input_number / int_val);
        var final_char = '';
        while (number > 0){
                final_char = final_char  + String.fromCharCode(number % 26 + 65);
                number = parseInt(number / 26)
        }
        final_char = final_char.ljust(4,'A');   
        return db_code + '_' + final_char.substring(0,2) + (input_number % int_val).toString().zfill(4) + final_char.substring(2,final_char.length) ;    
}

/**
* Given a strain barcode, this method returns the appropriate numeric id
* @param {string} barcode - The barcode of the strain
* @returns {integer} The strains numeric id
*/
UberStrainGrid.prototype.decodeBarcode=function(barcode){
        var code = barcode.split("_")[1];
        var prefix = code.substring(0,2);
        var suffix = code.substring(6,code.length);
        var charstring = prefix+suffix;
        var int_string =0;
        for (var idx=0;idx<charstring.length;idx++){
                int_string+= Math.pow(26,idx) * (charstring.charCodeAt(idx)-65)
        }
        int_string = int_string*10000
        return int_string + parseInt(barcode.substring(6,10));
}

String.prototype.ljust = function( width, padding ) {
        padding = padding || " ";
        padding = padding.substr( 0, 1 );
        if( this.length < width )
                return this + padding.repeat( width - this.length );
        else
                return this;
}




UberStrainGrid.prototype.callback= function(message){
        console.log(message)
}


/**
* Creates and shows a dailog allowing users to add/remove substrains to an Uber strain
*/
UberStrainGrid.prototype.addToUberStrainDialog= function(){
        var self = this;
        this.selectedRows={};
        this.refreshGrid();
        var div = $("<div>");        
        div.append($("<div> Please Select The Master Strain and Press OK</div><br>"));
  
        div.dialog({
            autoOpen: true,
            modal: false,
            title: "Add To Uber Strain ",
            width: 400,
            height:450,
            buttons: {
                "Close": function () {
                   
                    $(this).dialog("close");
                },
                 "OK": function () {
                    if (! div.data("master")){
                        var masterID =0;
                        for (var id in self.selectedRows){
                                masterID=id;
                                break;
                        }
                        if (! id){
                                return;
                        }
                        div.data("master",masterID);
                        var name = self.getStrainName(id);
                        div.append($("<b>Master Strain:"+name+"</b><br>Please select substrains to add to the master strain and press OK</br>"));
                        self.selectedRows={};
                        self.refreshGrid();
                    
                    }
                    else if (! div.data("substrains")){
                       
                        div.append($("<b>Substrains:</b><br>"));
                        var ul = $("<ul>");
                        var substrains = [];
                        for (var id in self.selectedRows){
                                substrains.push(id);
                                ul.append($("<li>"+self.getStrainName(id)+"</li>"));
                        }
                        div.data("substrains",substrains);
                        
                        div.append(ul);
                        div.append($("<span>").text("Press OK to add the substrains to the master"));
                          
                    }
                    else{
                        self.addToUberStrain(div.data("master"),div.data("substrains"));
                        $(this).dialog("close");
                    
                    }
                }
                
            },
            
            
            close: function () {
                $(this).dialog('destroy').remove();
            }
        });

};

/**
* Removes strains from the uberstrain and makes them independant.
* if a strain has no uberstrain, it will be ignored
* @param {array} IDs- A list of strains ids to remove from their master 
*/

UberStrainGrid.prototype.removeFromUberStrain= function(IDs){
    var uberCol = this.getColumnIndex('uberstrain');
    for (var index in IDs){
       
        var id = parseInt(IDs[index]);
        if (this.uberStrain[id]){
            continue;
        }
        var row = this.getRowIndex(id);
        var masterID = this.getValueAt(row,uberCol);
        var list = this.uberStrain[masterID]['substrains'];
        var remIndex =list.indexOf(id);
        list.splice(remIndex, 1);
        if (list.length === 0) {
            delete this.uberStrain[masterID]['substrains'];
        }
        this.setValueAt(row,uberCol,id);
        this.uberStrain[id]={state:'closed'};
        this.modelChanged(row,uberCol,masterID,id,0,true);
       
    }
    this.selectedRows={};
    this.refreshGrid();
};

UberStrainGrid.prototype.destroyUberStrain= function(id){
        var uberCol = this.getColumnIndex('uberstrain');
        var list = this.uberStrain[id]['substrains'];
        
        for(var i in list){
                var  sub_id = list[i];
                var row =this.getRowIndex(sub_id);
                this.setValueAt(row,uberCol,sub_id);
                this.uberStrain[sub_id]={state:'closed'};
                this.modelChanged(row,uberCol,id,sub_id,0,true);
                              
        }
        delete this.uberStrain[id]['substrains'];
        
        


}





/**Adds substrain to an uber strain
 * If one of the substrains is itself an uber strain then it and all
 * substrains will be added.
 * @param {integer} masterID -  The ID of the master strain
 * @param {list} substrainIDs - A list of all substrain IDs
 * @returns {string} 'success' if the creation was successful, otherwise
 * an error message
*/
UberStrainGrid.prototype.addToUberStrain= function(masterID,substrainIDs){
        //check master strain is not a substrain
        var uberCol = this.getColumnIndex("uberstrain");
        if (!this.uberStrain[masterID]){
                return "Master Strain is not a top level strain";
        }
        //list will contain a list with 0:substrain ID,1:substrain index 2:old master value
        var listOfSubstrains = [];
        for (var index in substrainIDs){
                var subID = substrainIDs[index];         
                var subRow = this.getRowIndex(subID);
                var subStrainInfo = [subID,subRow];
                var uber = this.uberStrain[subID];
                //not a top level strain therefore must remove it from
                //its current master
                if (!uber){
                        var masterRow = this.getRowIndex(subID);
                        var master = this.getValueAt(masterRow,uberCol);
                        subStrainInfo.push(master);
                        var sublist = this.uberStrain[master]['substrains'];
                        var remIndex = sublist.indexOf(subID);
                        sublist.splice(remIndex,1);
                        if (sublist.length ===0){
                              delete this.uberStrain[master]['substrains'] ;
                        }
                }
                //substrain has its own substrains 
                else {
                        subStrainInfo.push(subID);
                        var subs = uber['substrains'];       
                        if (subs){
                                for (var index2 in subs){
                                        var subSubRow = this.getRowIndex(subs[index2]);
                                        listOfSubstrains.push([subs[index2],subSubRow,subID]);
                                }
                                delete uber['substrains'];
                        }
                }
                listOfSubstrains.push(subStrainInfo);

        }
        var masterList = this.uberStrain[masterID]['substrains'];
        if (! masterList){
                masterList =[];
                this.uberStrain[masterID]['substrains']=masterList;
        }
        for (var index in listOfSubstrains){
                var id = listOfSubstrains[index][0];
                var row = listOfSubstrains[index][1];
                var oldValue = listOfSubstrains[index][2];
                //set the new value
                this.setValueAt(row,uberCol,masterID);
                delete this.uberStrain[id];
                //register the value has changed
                this.modelChanged(row,uberCol,oldValue,masterID,0,true);
                //add it uber substrain list
                masterList.push(parseInt(id));
        
        }
        //make sure top level is open
        this.uberStrain[masterID]['state']= "open";
        this.selectedRows={};
        this.refreshGrid();
        return 'success';
};




//load all info into the uberStrain dict before the data is loaded into the grid
UberStrainGrid.prototype.newDataLoaded= function(validate){
        this.uberStrain={};
        var uberColIndex= this.getColumnIndex('uberstrain');
        if (uberColIndex !==-1){
                var uber_to_sub = {};
                for (var n=0;n<this.getRowCount();n++){                
                        var uber = this.getValueAt(n,uberColIndex);
                        var id  = this.getRowId(n);
                        //is an uber strain
                        if (id === uber){
                                
                                this.uberStrain[id]={state:"closed"};
                        }
                        //must be a substrain - match it to uberstrain
                        else{
                                sub_list = uber_to_sub[uber];
                                if (!sub_list){
                                        sub_list=[];
                                        uber_to_sub[uber]=sub_list;
                                }
                                sub_list.push(id)
                        }
                }
                //load all substrain data into uberStrain
                for (uber in uber_to_sub){
                        //is an orphan substrain
                        if (!this.uberStrain[uber]){
                                continue;
                        }
                        this.uberStrain[uber].substrains=uber_to_sub[uber];
                }
        }
        //call super method
        SRAValidationGrid.prototype.newDataLoaded.call(this,validate);

};
//if row a row has substrains, they need removing
UberStrainGrid.prototype.rowRemovedListener =function(rowID){
        var row = this.getRowId(rowID);
        var info = this.uberStrain[rowID];
        if (info){
                if  (info['substrains']){
                        for (var index in info['substrains']){
                                this.removeRow(info['substrains'][index]);
                        }
                }
                delete this.uberStrain[rowID];
        }

}


//substrains will have white backgrouds
UberStrainGrid.prototype.getRowClass= function(rowID){       
        if (! this.uberStrain[rowID]){
                return "val-grid-white-background";
        }
        
        
}


UberStrainGrid.prototype.refreshGrid =function(){
        //data is the data to be displayed and in correct order
        //only display uber strains and sub strains of 'open' uberstrains
        //**
        if (!this.dataUnfiltered){
                this.dataUnfiltered=this.data;
        }
        var uberIndex = this.getColumnIndex('uberstrain');
        new_data=[];
        //change all tags to not visible in those curremtly displayed
        for (var index in this.data){
                var item  = this.data[index];      
                item.visible = false;
        }
        //loop through all strains picking out uber strains
        for (var index in this.data){
                var item  = this.data[index];
                var uber = item['columns'][uberIndex];
                item.visible = false
                //only show uber strains
                if (uber === item.id){
                        new_data.push(item);
                        var uber_details = this.uberStrain[item.id];
                        item.visible = true;
                        //add the substrains if uber strain is open
                        if (uber_details['state'] === 'open' && uber_details['substrains']){
                              sub_list = uber_details['substrains']
                              for (index in this.dataUnfiltered){
                                     var strain = this.dataUnfiltered[index];
                                     
                                     if (sub_list.indexOf(strain.id) !== -1){
                                        new_data.push(strain);
                                        strain.visible=true;
                                     }
                              
                              }                        
                        
                        }
                }
               
        }
        this.data=new_data;
        this.rowNumberListener(this.getCompleteRowCount(),this.getRowCount());
        //the order and filtering has changed - therefore need to call all callbacks
        //registered with this grid (in this case those from the right grid to match order/filtering)
        var keepOnPage =true;
        this.tableFiltered(keepOnPage);
        var self = this;
        //super call
        SRAValidationGrid.prototype.refreshGrid.call(this);

}   