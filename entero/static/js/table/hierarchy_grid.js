HierarchyGrid.prototype = Object.create(BaseExperimentGrid.prototype);
HierarchyGrid.prototype.constructor= HierarchyGrid;


function HierarchyGrid(name,config,species,scheme,scheme_name){
        BaseExperimentGrid.call(this,name,config,species,scheme,scheme_name);   
};


HierarchyGrid.prototype.getAllAtLevel = function(rowIndex,colIndex){
        var value = this.getValueAt(rowIndex,colIndex);
        var col_name= this.col_list[colIndex]['name']
        var query = col_name+"="+value;
        to_send={
                experiment_query:query
        }
        submitQuery(to_send);
}

HierarchyGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
        var self = this;
        var extramenu=[	{
                         name: 'Get at this level',
                         title: 'Get all this level',
                         fun: function () {               
                                 self.getAllAtLevel(rowIndex,colIndex);
                        }
                }        
        ];         
        return extramenu;
}