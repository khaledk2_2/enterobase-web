from flask_restful import Resource, reqparse, fields, inputs, marshal
from flask import abort, request, current_app
from flask_login import current_user
from entero import dbhandle, db,app, get_database, rollback_close_system_db_session
from entero.databases.system.models import User,UserJobs
from sqlalchemy.sql import or_,func
from sqlalchemy.orm import load_only
import json,ujson, requests, datetime
from entero.cell_app.tasks import import_sra,error_handler
from entero.jobs.jobs import AssemblyJob,network_job_classes
from entero.ExtraFuncs.workspace import get_access_permission


import os
current_jobs = ["SUBMIT","WAIT RESOURCE","QUEUE","RUNNING"]

class AssembleAPI(Resource):
    def put(self,dbname):
        dbase=get_database(dbname)
        if not dbase:
            return "dtabase %s is noy found"%dbname, 404 
        
        if  not current_user.is_authenticated():
            # need to put in method to add authorization for non-website calls
            return "No authorization",401
        accession = request.form.get('accessions')
        priority =request.form.get('priority')
        if not priority:
            priority=9
            
        if not accession:
            return "accessions required in put request ",400
        accs = accession.split(",")
        
        URI_BASE = app.config['CROBOT_URI']+"/head/set_priority"
        # Adding access permission check
        #allowed_users, permission=current_user.get_edit_meta_permissions(dbname)
        Strains=dbase.models.Strains
        Traces=dbase.models.Traces
        sts=[]
        recs = dbase.session.query(Strains).options(load_only('owner','release_date','id','created')).join(Traces).filter(Traces.accession.in_(accs)).all()
        for x in recs:sts.append(x.__dict__)
        permissions=get_access_permission(dbname, current_user.id,
                                          strains=sts, 
                                          return_all_strains_permissions=True)            
        for rec in recs:
            if permissions.get(rec.id)!=3:                
            #if not rec. in allowed_users and rec[0]!=current_user.id:  
                return "No authorization",401          
             
        for acc in accs:
            acc_names = acc.split(";")
            #check job is not already in queue
            acc_co = ",".join(acc_names)
            try:
                job =db.session.query(UserJobs).filter(UserJobs.accession.like(acc_co+"%"),UserJobs.status.in_(current_jobs)).first()
                #job already running , cannot increase its priority
                if job and job.status == 'RUNNING':
                    continue;
                if job:
                    URI = URI_BASE + "?job_tag=%i&priority=-9"%job.id
                    resp = requests.get(url = URI)
                    app.logger.info("Altering priority:"+URI);
                    data =None
                    try:
                        data = ujson.loads(resp.text)
                    except Exception:
                        app.logger.error("Cannot set job priority: response from crobot:\n%s" % resp.text)
                    if job.user_id==0:
                        job.user_id = current_user.id
                    db.session.add(job)
                    db.session.commit()
                    continue;

                ids = get_database(dbname).get_trace_ids_by_accession(acc_names)
                job =AssemblyJob(trace_ids=ids,
                                 database=dbname,
                                 workgroup="public",
                                 priority=priority,
                                 user_id=current_user.id)
                job.send_job()
            except Exception as e:
                rollback_close_system_db_session()
                app.logger.exception("Error in submitted Assemblies. error message: %s"%(e.message))
                return 'Error in submitted Assemblies',503
        return "Assemblies submitted",201
    
  
class NservAPI(Resource):
    def put(self,dbname):
        dbase=get_database(dbname)
        if not dbase:
            return "dtabase %s is noy found"%dbname, 404
        if  not current_user.is_authenticated():
            # need to put in method to add authorization for non-website calls
            return "No authorization",401
        scheme = request.form.get('scheme')
        if not scheme:
            return "accessions required in put request ",400
        priority = request.form.get('priority')  
        ass_ids = request.form.get('assembly_ids')
        if not ass_ids:
            return "accessions required in put request ",400
        priority = request.form.get('priority')
        
        if not priority:
            priority=9
        allowed_users, permission=current_user.get_edit_meta_permissions(dbname)
        Assemblies = dbase.models.Assemblies
        Schemes=dbase.models.Schemes
        ids_list = ass_ids.split(",")

        try:
            ###############################################################################
            #check permission, using centeral permission check
            ###############################################################################    
            Strains=dbase.models.Strains
            strains=dbase.session.query(Strains).options(load_only('owner','release_date','id','created')).filter(Strains.best_assembly.in_(ids_list)).all()            
            sts=[]
            for x in strains:sts.append(x.__dict__)
            permissions=get_access_permission(dbname, current_user.id,
                                              strains=sts, 
                                              return_all_strains_permissions=True)     
            #if not permission:               
            for strain in strains:
                #if not strain[0] in allowed_users and strains[0]!=current_user.id:
                if permissions.get(strain.id)!=3:
                    return "No authorization",401
           
            
            assemblies  = dbase.session.query(Assemblies).filter(Assemblies.id.in_(ids_list)).all()
            
            
            scheme_obj =dbase.session.query(Schemes).filter(Schemes.description==scheme).one()
            #bit of a hack , but the class key is usually the same as the pipeline
            #although it is possible for it to be different (if a pipeline is serving two purposes)
            key = scheme
            if scheme_obj.param['pipeline']=='nomenclature':
                key='nomenclature'
            scheme_class = network_job_classes[key]
            
            for assembly in assemblies:
                #I have committed the next two attributes as they neve used
                #barcode = assembly.barcode
                #location = assembly.file_pointer                               
                
                job = scheme_class(database=dbname,
                                                scheme =scheme_obj,
                                                assembly_barcode =assembly.barcode,
                                                assembly_filepointer=assembly.file_pointer,
                                                user_id=current_user.id,
                                                priority=-9,
                                                workgroup="user_upload")
                job.send_job()
            dbase.session.close()
        except Exception as e:
            dbase.rollback_close_session()
            app.logger.exception("****Error in sending nomenclature job for assembly in %s databae, error message: %s"%(dbname, e.message))
            return "Error in sending Nomencalture job",400
        return "Nomen submitted",201 
    
