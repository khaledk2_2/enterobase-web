from entero import celery,app,db,dbhandle, get_database, rollback_close_connection, rollback_close_system_db_session
from datetime import datetime
from entero.databases.generic_models import decode
from entero.databases.system.models import UserUploads
import sys,ujson,requests,os
from entero.databases.system.models import User,UserJobs, UserPreferences
from entero.ExtraFuncs.workspace import get_analysis_object, Analysis
from sqlalchemy.orm.attributes import flag_modified

from sqlalchemy.orm.exc import  NoResultFound

class LocalJob(object):
    '''Classes should inherit from this if they are to run locally and the logic
    should be placed in the *process_job* method, which is actually called
    by *send_job* .If celery is runnng  (app.config['USE_CELERY'] is True),
    the *process_job* method  will run in the celery thread
    This can overidden for individual jobs by setting  member use_celery.

    .. code-block:: python

        class Job1(LocalJob):
            def __init__(self.**kwargs):
                param1=kwargs.get("param1)
                super(Job1,self).__init__(**kwargs)
            def process_job(self):
                #do stuff with self.param1
    '''
    def __init__(self,**kwargs):
        '''This method should be overidden and any values required
        for the job stored in the object
        '''
        self.use_celery=app.config['USE_CELERY']
        super(LocalJob,self).__init__(**kwargs)
       
    def send_job(self):
        '''Method called to execute the job'''

        if self.use_celery:
         
            kwargs=vars(self)
            nwargs={}
            for key in kwargs:
                if not key.startswith("_"):
                    nwargs[key]=kwargs[key]
            send_to_celery.apply_async(args=[nwargs],queue="entero")
           
        else:
            self.process_job()
    
        return True

  
    def process_job(self):
        '''This method should be overidden with the the job's logic'''
        pass



class RemoteJob(object):
    '''This class should be subclassed if the job is to be sent to a remote server
    and the result passed back at a later date. As well as *send_job* and *process_job*,
    the subclass should also have a *job failed* method'''
    def __init__(self,**kwargs):
        '''When data is a available, from the remote server, one of the aguments
        should be 'data' with a dictionary of the returned data. If is is to be constructed
        for sending the parameters rewuired for sending the job should be given'''
        self.results=kwargs.get("data")
        #if multiple inheritence need to pas **kwargs else it causes an exception
        try:
            super(RemoteJob,self).__init__(**kwargs)
        except:
            super(RemoteJob,self).__init__()

    def send_job(self):
        '''This method should be oveidden with the logic for sending the job with
        the paramters given in the constructor
        '''
        pass

    def process_job(self):
        '''This method should be overidden with the logic for processing
       the returned data, which is in self.results 
        '''
        pass


    def update_job(self):
        '''This method is called to process the job if it has been created with'''
        stat = self.results['status']
        if  stat.startswith("FAILED") or stat.startswith("KILLED"):
            self.job_failed()
        elif stat == 'COMPLETE':
            self.process_job()



class CRobotJob(RemoteJob):
    '''This class should be subclassed, where construction arguments are converted
    to params,inputs and reads required by crobot. The *send_job* method will 
    send the job to crobot and store information in user_jobs. *process_job*, *job_sent*
    and *job_failed* need to be provided by the subclass.''' 
    def __init__(self,**kwargs):
        '''If constructed with a data argument, the constructor will reconstitute
        the job from this information.This class and subclasses should only be constructed
        with data returned from crobot or the job id using :meth:`get_crobot_job`
        
        If constructed with other parameters, the following are required:-
        
        * **database**
        * **inputs,params,reads** - as required by crobot
        * **tag** the value that goes in acession field in the user_jobs database
        * **pipeline** the crobot pipeline
        * **type** the job type (stored in pipeline in the user_jobs table)
        
        The  following are optional

        * **user_id** - 0 (crobot) by default
        * **priority** 9 (lowest) by default
        * **workgroup** backend by default
        * **user_name** taken from user_id or crobot if not given
        '''
        if kwargs.get("data"):
            data=kwargs['data']
            if data.get('log'):
                #not already converted to dictionary
                if not type(data['log']) is dict:
                    try:
                        data['log']=ujson.loads(data['log'])
                    except:
                        data['log']=None
            self.reads=data.get("reads")
            self.job_id=int(data['tag'])
            self.job_db = kwargs['job_db']
            self.user_id=self.job_db.user_id
            self.database= self.job_db.database
            self.tag=self.job_db.accession
            self.params=data.get('params')
            self.inputs = data.get("inputs")
            self.user_name=data['usr']
            self.pipeline = data['pipeline']
            self.priority=data['priority']
            self.workgroup=data['workgroup']
            stat = self.job_db.status
            
            if  stat.startswith("FAILED") or stat.startswith("KILLED"):
                self.status="FAILED"
            elif stat == 'COMPLETE':
                self.status='COMPLETE'
            else:
                self.status="QUEUED"
        else:
            self.database=kwargs.get("database")
            self.inputs=kwargs.get("inputs")
            self.reads=kwargs.get("reads")
            if kwargs.get("params"):
                if not hasattr(self,"params"):
                    self.params={}
                    for key in kwargs['params']:
                        self.params[key]=kwargs['params'][key]

            self.user_id=kwargs.get("user_id",0)
            self.database=kwargs.get("database")
            self.user_id=kwargs.get("user_id") 
            self.priority=kwargs.get("priority",9)
            self.workgroup=kwargs.get("workgroup","backend")
            if kwargs.get("tag"):
                self.tag=kwargs.get("tag")
            self.user_name=kwargs.get("user_name")
            self.pipeline=kwargs.get("pipeline")
            self.satus="NEW"
        super(CRobotJob,self).__init__(**kwargs)

    def send_job(self,retries =1):
        '''Sends the job to crobot and stores the job details in the 
        user_jobs-table, should not need to be overidden'''
        #do we need a call to the database to get the users name
        if  self.user_name and self.user_id==None:
            user = db.session.query(User.id).filter_by(username=self.user_name).one()
            self.user_id=user.id
        elif self.user_id<>None and not self.user_name:
            user = db.session.query(User.username).filter_by(id=self.user_id).one()
            self.user_name=user.username
        elif self.user_id==None and not self.user_name:
            self.user_name="crobot"
            self.user_id=0
        #add all the parameters
        URI = app.config['CROBOT_URI']+"/head/submit"
        params = {
            "source_ip":app.config['CALLBACK_ADDRESS'],
            "usr":self.user_name,
            "query_time":str(datetime.now()),
            "workgroup":self.workgroup,
            "_priority":self.priority,
            "pipeline":self.pipeline 
        } 
        if  self.inputs:
            params['inputs']=self.inputs
        params['params']=self.params
        if self.reads:
            params['reads']=self.reads
        success=False
        for tries in range(0,retries):
            try:
                #send the job and get the reply
                resp = requests.post(url = URI, data = {"SUBMIT":ujson.dumps(params)})
                if resp.status_code == 200:
                    data = ujson.loads(resp.text)
                    #create a record in user_jobs table
                    #a check to the data needs to be added before using it
                    #I the server sometimes sends an error message something went wrong
                    if isinstance(data, unicode) or isinstance(data, str):
                        app.logger.error("reuested for send_job failed, error message: %s"%data)
                        return success

                    self.job_id=data['tag']
                    try:
                        job = UserJobs(id=data['tag'],pipeline=self.type,date_sent=data["query_time"],database=self.database,
                               status=data["status"], accession = str(self.tag),user_id=self.user_id)
                    except Exception as e:
                        raise Exception("job id is: %s, data is: %s, self log: %s, error message: %s"%(self.job_id, data, self.tag, e.message))
                    db.session.add(job)
                    db.session.commit()
                    self.job_db=job
                    success=True
                    break
                else:
                    app.logger.error("Error from CRobot sending %s job\nparams: %s\ninputs:%s, respond: %s" %(self.type,str(self.params),str(self.inputs), resp))
            except Exception as e:
                app.logger.exception("Problem sending %s job\nparams: %s\ninputs:%s, error message: %s" %(self.type,str(self.params),str(self.inputs), e.message))
                rollback_close_system_db_session()
        #call any handlers
        self.job_sent(success)
        return success

    def update_job(self):
        '''Updates the job with the data it was constructed with calls to
        *job_failed* or *process_job* depending of the status '''
        stat = self.results['status']
        try:
            if stat.startswith("FAILED") or stat.startswith("KILLED"):
                self.status="FAILED"
                self.job_db.status=stat
                db.session.commit()
                self.job_failed()
                if stat == 'KILLED_INSUFFIFIENT_INPUT' :
                    read_info = self.results['inputs'].get('read', [])
                    if len(read_info) == 2 :
                        spe_db = get_database(self.job_db.database)
                        Traces = spe_db.models.Traces
                        trace = spe_db.session.query(Traces).filter(Traces.accession == read_info[0]).first()
                        
                        trace.seq_library = 'Paired (Single in NCBI)'
                        
                        spe_db.session.commit()
                        
            elif stat == 'COMPLETE':
                self.status='COMPLETE'
                self.job_db.status="COMPLETE"
                db.session.commit()
                self.process_job()
                
            else:
                self.status="QUEUED"
                self.job_db.status=stat
                db.session.commit()
            return stat
        except Exception as e:
            rollback_close_system_db_session()
            app.logger.exception("Update job failed, error message: %s"%e.message)
            return "FAILED"

    def set_reads(self):
        '''For an object with traces, this method will create the reads,inputs
        and params required by crobot.

        Returns the trace acessions in the the format
        * sra reads comma delimited accession numbers
        * user reads  trace_accession:#read1_name#read2_name
        '''
        pos=1
        platform_list=[]
        read_dict={}
        #the accession numbers in the case of user reads
        #format is trace_accession:#read1name#read2name etc
        acc_numbers = ""
        user_reads=False
        for trace in self.traces:
            
            if trace.accession.startswith("traces"):
                user_reads=True
            read_info= None
            if user_reads:
                reads = trace.read_location.split(",")
                read_info= [trace.barcode]
                acc_numbers+=","+trace.accession+":"
                for item in reads:
                    read_info.append(item)
                    acc_numbers+="#"+(item[item.rindex('/')+1:])
            else:
                read_info=trace.accession
                acc_numbers+=","+trace.accession
            append="";
            if pos<>1:
                append=str(pos)
            pos+=1
            read_dict["read"+append]= read_info
            plat = app.config['PLATFORM_TO_PARAM'].get(trace.seq_platform)
            if plat==None:
                app.logger.warning("seq platform:%s does not exits"%(trace.seq_platform))
            platform_list.append(plat)
        
        if user_reads:
            self.inputs=read_dict
            self.params['clean_reads']=False
        else:
            self.reads=read_dict
            self.params['clean_reads']=True
            self.inputs=None
        self.params['library']=platform_list

        return acc_numbers[1:]

class AssemblyJob(CRobotJob):
    '''Creates an assembly job, either from parameters (see below) or from the data returned
    from crobot or the id of the job in the user_jobs database.For the latter two use :meth:`get_crobot_job`

    :param trace_ids: The id of the traces for the assembly
    :param database: The name of the database
    :param priority: The job priority (optional default 9 )
    :param workgroup: The queue to send job to (optional - default backend)
    :param user_id: optional (default 0 = crobot)
    ''' 
    def __init__(self,**kwargs):
        super(AssemblyJob,self).__init__(**kwargs)

        self.type="QAssembly"
        self.pipeline="QAssembly"
        dbase = get_database(self.database)
        try:
            if not kwargs.get("data"):
                #work out species name
                species_name = app.config['DB_CODES'][self.database][0].title()

                if species_name == 'Miu':
                    species_name = 'Salmonella'

                self.params={"species":species_name}
                trace_ids=kwargs['trace_ids']
                #get the trace records
                Traces = dbase.models.Traces
                self.traces = dbase.session.query(Traces).filter(Traces.id.in_(trace_ids)).all()
                self.strain_id = self.traces[0].strain_id
                #create the assembly
                self.create_assembly()
                self.params['prefix']= self.assembly.barcode
                #set parameters for the reads and get tag for traces
                self.tag =self.set_reads()

            #all params will be re-created from the crobot returned data
            #only assembly needs to be obtained
            else:
                #get the assembly
                Assemblies = dbase.models.Assemblies
                ass_barcode= kwargs['data']['params']['prefix']
                self.assembly = dbase.session.query(Assemblies).filter_by(barcode=ass_barcode).first()
        except Exception as e:
            app.logger.exception("Error while initializing AssemblyJob class, error message: %s"%e.message)
            dbase.rollback_close_session()


    def create_assembly(self):
        '''Helper method, which creates a fresh assembly
        '''
        dbase = get_database(self.database)
        try:
            assembly =  dbase.models.Assemblies(status='Queued',user_id=self.user_id)
            dbase.session.add(assembly)
            dbase.session.commit()
            assembly.barcode = dbase.encode(assembly.id, self.database, 'assemblies')
            for trace in self.traces:
                assembly.traces.append(trace)
            dbase.session.commit()
            self.assembly=assembly
        except Exception as e:
            app.logger.exception("Error in create_assembly, error message: %s"%e.message)
            dbase.rollback_close_session()
    
    def send_job(self, retries=1):
        '''Checks to see if the job is being resent and if so creates a new assembly
        before calling :func:`CRobotJob.send_job`'''
        dbase=get_database(self.database)
        Traces=dbase.models.Traces
        #need to get traces from the job db
        if not self.assembly or self.assembly.status<>"Queued":
            accessions = self.job_db.accession.split(",")
            acc_list=[]
            for acc in accessions:
                if "#" in acc:
                    acc = acc.split("#")[0]
                    acc_list.append(acc)
            self.traces = dbase.session.query(Traces).filter(Traces.accession.in_(acc_list)).all()
            self.create_assembly()
            
        # WVN 28/2/18 Tests expect send_job in AssemblyJob class to have a return value now
        return super(AssemblyJob,self).send_job(retries)

   
    def job_sent(self,success):
        '''Deletes the assembly if the job cannot be sent (needs to be created in 
        the first place to get the barcode for crobot)

        If the job is sent and associated strain has no best assembly, this
        will become its best assembly
        '''
        dbase=get_database(self.database)
        try:
            if not success:
                #delete the assembly
                dbase.session.delete(self.assembly)
                dbase.session.commit()
            else:
                #make it the strains best assembly if not one already and add job id
                strain = dbase.session.query(dbase.models.Strains).filter_by(id= self.strain_id).one()
                if not strain.best_assembly:
                    strain.best_assembly = self.assembly.id
                self.assembly.job_id=self.job_id
                self.assembly_status="Queued"
                self.assembly.status="Queued"
                dbase.session.commit()
        except Exception as e:
            app.logger.exception("job_sent failed, error message: %s"%e.message)
            db.rollback_close_session()

        return success

    def process_job(self):
        '''Updates the assemblies and strains table. Also if any traces
        are user reads, the status of the userupload entries are changed to 'Assembled'
        '''
        success = True
        badReads = 0
        dbase = get_database(self.database)
        send_exception=True

        try:
            outputs  = self.results['outputs']
            #put the assembly location in the jobs database
            ass_location = str(outputs['assembly'][1])
            #KM: check if the assembly location file exists, if not it will throw an error
            # it will send an email to zhemin to checkt that
            #zhemin email needs to be determined from the configuration file instead of hard coding as it is right now
            #THis shold be done using new keyword such "NServ_Email"
            if not ass_location or not os.path.exists(ass_location):
                #the following changes has been made by Zhemin to check the resaons of assembly failing
                # and if it is  failed  due to "Bad read sequences." it will assign the assembly output file to ''
                #and continue working with assembly update
                try :
                    log =self.results['log']
                    ass = log["assembly"]
                    reasons =ass.get('Reason')
                    if len(reasons) > 0 :
                        badReads = 1
                        if reasons[0] == "Bad read sequences." :
                            badReads = 2
                except :
                    pass
                if badReads < 2 :
                    error_message="assembly file (%s) is not valid or exist" % ass_location
                    if badReads < 1 :
                        from entero.entero_email import sendmail
                        text=error_message+'\n'+str(self.results)
                        sendmail(me='enterobase@warwick.ac.uk', to=['zhemin.zhou@warwick.ac.uk'],
                                 subject='Assembly Job Issue', txt=text)
                    send_exception=False
                    raise Exception(error_message)
                
                else:
                    self.job_db.output_location = ''
            else:
                self.job_db.output_location=ass_location

            #update version
            if not self.assembly:
                raise Exception("The job does not have an assembly attribute")
            self.assembly.pipeline_version=self.results['version']
            #update any user uploaded reads
            if not self.change_user_read_status():
                    raise Exception("change_user_read_status failed")

            self.assembly.user_id=self.user_id
            
            log =self.results['log']
            if not log:
                raise Exception ("process_job failed, log is not included in the results")
            try:
                #KM 25/07/2018
                #I think log is json string and not dict,I have added getting log type
                #so if it is json string, it is needed to load it before trying to use it
                ass = log["assembly"]
            except Exception as e:
                raise Exception("Could not get assembly from log %s, log type is: %s, error message: %s"%(log, type(log), e.message))

            #work out the coverage
            coverage = None
            try:
                bases = log['reads']['bases']
                length = log['assembly']['Total_length']
                coverage = bases/length
                coverage = float(format(coverage, '.1f'))
            except :
                coverage= None
            if coverage:
                self.assembly.coverage=coverage

            self.assembly.n50 = ass.get("N50")
            self.assembly.total_length =  ass.get("Total_length")
            self.assembly.low_qualities = ass.get("Low_qualities")
            self.assembly.contig_number =  ass.get("Contig_num")
            self.assembly.file_pointer = ass_location
            
            sp =ass.get("Species")
            #add check to sp items to be sure it is not a bool
            # as it has been sent as bool sometimes from crobot
            # I will clarify it with Zhemin later
            # K.M. 23/07/2019
            try:
                if sp and isinstance(sp, list) and isinstance(sp[0], list) and len(sp[0])>=2:
                    self.assembly.top_species=str(sp[0][0])+";"+str(sp[0][1])+"%"
            except Exception as e:
                app.logger.exception("Cannot process Assembly job id: %i error: %s, sp is : %s" % (self.job_id, e.message, sp))

            #get the failiure reasons
            if not ass.get("Accept"):
                self.assembly.status="Failed QC"
                reasons =ass.get('Reason')
                if reasons and len(reasons)>0:
                    reason_list=[]
                    for reason in reasons:
                        reason_list.append(app.config['CONTIG_STATS_MAP'].get(reason))
                    self.assembly.add_other_data({'reason_failed':reason_list})
                dbase.session.commit()
                
            else:
                self.assembly.status="Assembled"
                #make it the strains best assembly
                #can add logic to see if it should be replaced
                if not self.assembly.traces:
                    #dbase.session.delete(self.assembly)
                    #dbase.session.commit()
                    raise Exception("Traces missing from database ass_id %s" %self.assembly.barcode)
                else:
                    stid =self.assembly.traces[0].strain_id
                    strain = dbase.session.query(dbase.models.Strains).filter_by(id=stid).first()
                    strain.best_assembly=self.assembly.id
                    dbase.session.commit()
                    #send schemes if user uploaded
                    if self.user_name <> "crobort":
                        send_all_jobs_for_assembly(self.assembly,self.database,
                                                                 user_id=self.user_id,
                                                                 priority=-9,
                                                                 workgroup="user_upload")
        except Exception as e:
            dbase.session.rollback()
            if send_exception:
                app.logger.exception("Cannot process Assembly job id: %i error: %s" % (self.job_id, e.message))
            else:
                app.logger.warning("Cannot process Assembly job id: %i error: %s" % (self.job_id, e.message))
                
            success=False
        #KM job_processed method conatined only pass,
        #so in case of success is False, it will do nothing
        #so I have added check for success varaible inside this method and in case of it is False,
        # it will call job_failed method
        self.job_processed(success)
        return success
    
    def change_user_read_status(self):
        '''Helper method - if any traces are user reads,  the status of the
        userupload entries are changed to 'Assembled'
        '''
        has_user_reads=False
        try:
            for trace in self.assembly.traces:
                if trace.read_location:
                    has_user_reads=True
                    paths=trace.read_location.split(",")
                    for path in paths:
                        filename = os.path.basename(path)
                        foldername = os.path.split(path)[0]
                        rec = UserUploads.query.filter(UserUploads.type=="reads",
                                                       UserUploads.species==self.database,
                                                       UserUploads.file_name==filename,
                                                       UserUploads.file_location==foldername,
                                                       UserUploads.status<>'Assembled'
                                                       ).first()
                        if rec:
                            rec.status = "Assembled"
                            db.session.add(rec)
            if has_user_reads:
                db.session.commit()
            return  True
        except Exception as e:
            rollback_close_system_db_session()
            app.logger.exception("change_user_read_status failed, error message: %s"%e.message)
            return False

    def job_failed(self):

        '''If the job has failed, the assembly is deleted and if 
        it is the 'best_assembly' of a strain, it will be removed
        '''
        dbase = get_database(self.database)
        if not dbase:
            return
        try:
            #delete the assembly if it exists
            if self.assembly:
                strain = dbase.session.query(dbase.models.Strains).filter_by(best_assembly=self.assembly.id).first()
                if strain:
                    #update the strain - this is no longer the best assembly
                    strain.best_assembly=None
                #dbase.session.delete(self.assembly)
                self.assembly=None                
                dbase.session.commit()
        except Exception as e:
            dbase.rollback_close_session()
            ass_id=''    
            try:
                if self.assembly:
                    ass_id=self.assembly.id                                        
            except:
                pass            
            app.logger.exception("AssemblyJob/job_failed/deleting assembly with id %s is failed, error message: %s" %(ass_id,e.message))


    def job_processed(self,success):
        # K.M. this method was not implemented, it was has only pass
        # In case of process_job failed for any reason, the success variable will be set to False,
        # In such case it should call job_failed method
        if not success:
            self.job_failed()


class QA_evaluation_Job(CRobotJob):
    # K.M 8/5/2019
    # this job is used to check the assembly QC in case of the user has uploaded a whole genome
    # It can be used in the future to check the uploaded assembly files from Local EnteroBase
    # upload it to Enterobase)
    def __init__(self, **kwargs):
        super(QA_evaluation_Job, self).__init__(**kwargs)
        self.type = "QA_evaluation"
        self.pipeline = "QA_evaluation"
        #Set the scheme name which needs to be the same as display database name but the first letter needs to be capital
        #The only excption is ecoli, it should be Elcoi
        #Also, sandbox database has the same scheme name as salmonella
        #K.M. 12/12/2020
        if self.database=='miu':
            self.params = {"scheme": "Salmonella"}
        elif self.database=='ecoli':
            self.params = {"scheme": "Ecoli"}
        else:
            dbs = app.config['ACTIVE_DATABASES']
            self.params = {"scheme": (dbs[self.database][0]).capitalize()}
        if not kwargs.get("data"):
            self.assembly = kwargs['assembly']
            self.inputs = {"assembly": self.assembly.file_pointer}
            self.tag = self.assembly.id
        else:
            self.data = kwargs['data']

    def update_job(self):
        status="Completed"
        assembly_id = self.job_db.accession
        dbase = get_database(self.database)
        Assemblies = dbase.models.Assemblies
        self.assembly = dbase.session.query(Assemblies).filter(Assemblies.id == assembly_id).first()
        self.assembly.pipeline_version = self.data.get("version")
        if self.data.get('log'):
            self.assembly.n50 = self.data['log'].get("N50")
            self.assembly.total_length = self.data['log'].get("Total_length")
            self.assembly.low_qualities = self.data['log'].get("Low_qualities")
            self.assembly.contig_number = self.data['log'].get("Contig_num")
            sp = self.data['log'].get("Species")
            try:
                if sp and isinstance(sp, list) and isinstance(sp[0], list) and len(sp[0]) >= 2:
                    self.assembly.top_species = str(sp[0][0]) + ";" + str(sp[0][1]) + "%"
            except Exception as e:
                app.logger.exception(
                    "Cannot process Assembly Control job id: %i error: %s, sp is : %s" % (self.job_id, e.message, sp))

            accepted=self.data.get('log').get("Accept")
            if  not accepted:
                #Check the status from the job strain which indicate if the assembly file fufil our assembly control criteria
                #If so, it will all other jobs for assembly such as rmlst, gwmlst, etc..
                #K.M. 15/12/2020
                self.assembly.status = "Failed QC"
                reasons = self.data.get('Reason')
                if reasons and len(reasons) > 0:
                    reason_list = []
                    for reason in reasons:
                        reason_list.append(app.config['CONTIG_STATS_MAP'].get(reason))
                    self.assembly.add_other_data({'reason_failed': reason_list})
                dbase.session.commit()
            else:
                self.assembly.status = "Assembled"

                if not self.assembly.traces:
                    raise Exception("Traces missing from database ass_id %s" % self.assembly.barcode)
                else:
                    stid = self.assembly.traces[0].strain_id
                    strain = dbase.session.query(dbase.models.Strains).filter_by(id=stid).first()
                    strain.best_assembly = self.assembly.id
                    dbase.session.commit()
                    if self.user_name <> "crobort":
                        send_all_jobs_for_assembly(self.assembly, self.database,
                                                   user_id=self.user_id,
                                                   priority=-9,
                                                   workgroup="user_upload")

        self.job_db.status = self.data.get('status')
        db.session.commit()
        return status

    def job_sent(self, success):
        return success



class AssemblyBasedJob(object):
    '''This should be inherited by all  'scheme'jobs' i.e. those jobs
    which process an assembly for a scheme. It provides the *job_failed*, *job_processed* and
    *job_sent* methods, which update the assembly_lookup table. It also provides a *get_lookup*
    method, which gets (creates it of necessary) the lookup object, which you can use to store
    results.Classes inheriting this need to be constructed with the following arguments:-
    
    * **scheme** The SQLAchemy object for the scheme
    *  **assembly_barcode**
    * **assembly_filepointer**
    * **database**

    Or can be constructed using  :meth:`get_crobot_job`
    '''
    def __init__(self,**kwargs):
        self.__lookup=None
        scheme = kwargs.get('scheme')
        
        if kwargs.get("assembly_barcode"):
            self.assembly_barcode=kwargs.get("assembly_barcode")
        if scheme:
            custom_params=scheme.param.get("params")
            if custom_params:
                if not hasattr(self,"params"):
                    self.params={}
                    for  key in custom_params:
                        if custom_params[key]=="{barcode}":
                            self.params[key]=self.assembly_barcode
                        else:
                            self.params[key]=custom_params[key]
            pipeline =scheme.param.get("pipeline")
            if pipeline:
                self.pipeline=pipeline
            self.scheme_id =scheme.id
        if kwargs.get("database"):
            self.database=kwargs.get("database")
        if kwargs.get("assembly_filepointer"):
            self.assembly_filepointer=kwargs.get("assembly_filepointer")
        super(AssemblyBasedJob,self).__init__()
  
    def get_lookup(self,not_create=False,force=False):
        ''' Gets the lookup record for the job (assembly and scheme combination) if one exists,
        otherwise a new one will be created and returned. The lookup is stored, so multiple
        calls to this method will not cause wasted database queries.
        
        :param not_create: If True (default False) then if no lookup exists i.e. this job has 
            not been called before , no lookup will be created and None will be returned
        '''
        if self.__lookup and not force:
            return self.__lookup

        assembly_id= decode(self.assembly_barcode)
        dbase=get_database(self.database)
       
        # Create Database Models
        Lookup = dbase.models.AssemblyLookup
        initial_data= {"fail_count":0,
                         "queued_count":0,
                         "complete_count":0,
                         "job_count":0}
        lookups = dbase.session.query(Lookup).filter_by(scheme_id=self.scheme_id,assembly_id=assembly_id).all()
        if  len(lookups)==0:
            if not_create:
                return None
            lookup=Lookup(scheme_id=self.scheme_id,assembly_id=assembly_id)
            lookup.other_data=initial_data
        else:
            lookup=lookups[0]
        #add empty if results is none
        results = lookup.other_data.get("results")
        if not results:
            lookup.other_data['results']={}
        #some legacy records do not have these
        if not lookup.other_data.get("fail_count"):
            lookup.other_data.update(initial_data)
        #the only way for sql achemy to update the json
        lookup.other_data=dict(lookup.other_data)
        dbase.session.add(lookup)
        self.__lookup=lookup
        return lookup 

    def job_sent(self,success):
        '''Will update the lookup entry depending on whether
        the job could be sent or not'''
        dbase=get_database(self.database)
        try:
            lookup=self.get_lookup()
            lookup.other_data['job_count']+=1
            if not success:
                lookup.other_data['fail_count']+=1
                if lookup.status<>'COMPLETE':
                    lookup.status="FAILED"
            else:
                lookup.other_data['queued_count']+=1
                lookup.status="QUEUED"
                if hasattr(self,"job_id"):
                    lookup.other_data['job_id']=self.job_id
            dbase.session.commit()
            #refresh
            lookup.other_data=dict(lookup.other_data)
        except Exception as e:
            app.logger.exception("job_sent failed, error message: %s"%e.message)
            dbase.rollback_close_session()
        return success

    def job_failed(self):
        '''Will update the lookup entry depending on whether
        the job failed at the crobot end or not'''
        dbase = get_database(self.database)
        try:
            lookup=self.get_lookup()
            if lookup.status<>'COMPLETE':
                lookup.status='FAILED'
            if lookup.other_data['queued_count']>0:
                lookup.other_data['queued_count']-=1
            lookup.other_data['fail_count']+=1
            lookup.other_data=dict(lookup.other_data)
            dbase.session.commit()
        except Exception as e:
            app.logger.exception("job_failed failed, error message: %s" % e.message)
            dbase.rollback_close_session()

    def job_processed(self,success,version="ND"):
        '''Will update the lookup entry depending on whether
        the job was processed'''  
        lookup=self.get_lookup()
        try:
            if lookup.other_data['queued_count']>0:
                lookup.other_data['queued_count']-=1
            if success:
                lookup.status="COMPLETE"
                lookup.other_data['complete_count']+=1
                lookup.version=version
            else:
                if lookup.status<>"COMPLETE" or lookup.st_barcode==None:
                    lookup.status="FAILED"
                    lookup.other_data['fail_count']+=1
                    lookup.other_data['invalid']=True
            lookup.other_data=dict(lookup.other_data)
        except:
            pass
        get_database(self.database).session.commit()

       
    
    def get_current_version(self):
        '''Returns the current version for this scheme,assembly combination
        or None if it does nor exist
        '''
        lookup = self.get_lookup(not_create=True)
        if not lookup:
            return None
        return lookup.version
        

class RefMaskerJob(CRobotJob,AssemblyBasedJob):
    '''Will send an assembly to be processed by crobot's refMasker
    i.e. highlight all the repetetive regions. The pointer to thegff file describing the
    regions is stored in the assembly_lookup table

    :param database:
    :param assembly_barcode:
    :param assembly_filepointer:
    :param scheme: The SQLAlchemy object for the refMasker scheme
    '''    
    def __init__(self,**kwargs):
        super(RefMaskerJob,self).__init__(**kwargs)
        if not self.results:
            self.params={'prefix':self.assembly_barcode}
            self.pipeline='refMasker'
            self.inputs={'reference':self.assembly_filepointer}
            self.tag=self.assembly_barcode
            self.type="refMasker"
        else:
            self.assembly_barcode=self.job_db.accession.split(":")[0] #leagacy
            dbase = get_database(self.database)
            Schemes = dbase.models.Schemes
            scheme = dbase.session.query(Schemes).filter_by(description="ref_masker").one()
            self.scheme_id=scheme.id
        
    def process_job(self):
        lookup=self.get_lookup()
        lookup.other_data["file_location"]=self.results['outputs']['repeat'][1]
        self.job_processed(True,self.results.get("version"))





class NomenclatureJob(CRobotJob,AssemblyBasedJob):
    def __init__(self,**kwargs):
        '''Will send an assembly to be processed by crobots nomenclature pipeline
        and store the results in the lookup table. the job can either be constructed
        with the parameters below or using :meth:`get_crobot_job`

            :param database:
            :param user_id: oprional (0 by default)
            :param assembly_barcode:
            :param assembly_filepointer:
            :param scheme: The SQLAlchemy object for the r scheme
        ''' 

        #need to get scheme_id
        if kwargs.get("data"):
            #need to get the scheme_id based on scheme plus assenbly_barcode
            data= kwargs['data']
            dbase=get_database(kwargs['job_db'].database)
            Schemes = dbase.models.Schemes
            scheme_name=data['params']['scheme']
            scheme = dbase.session.query(Schemes).filter(Schemes.param['scheme'].astext ==scheme_name).one()
            self.scheme_id =scheme.id
            kwargs['assembly_barcode']=data['params']['genome_id']
            self.type="nomenclature"+":"+data['params']['scheme']
            

        #will just have scheme,assembly_barcode,assembly_filepointer,database,user_id 
        else:
            scheme = kwargs['scheme']
            kwargs['params']={
                "genome_id":kwargs['assembly_barcode'],
                "scheme":scheme.param['scheme']
            }
            kwargs['inputs']={"genome_seq":kwargs['assembly_filepointer']}
            kwargs['tag']=kwargs['assembly_barcode']
            kwargs['pipeline']="nomenclature"
            self.type="nomenclature"+":"+scheme.param['scheme']
        #stored in the pipeline of the jobs database
        
       
        super(NomenclatureJob,self).__init__(**kwargs)


 
    def process_job(self):
        '''Stores the ST_barcode and gff output in the lookup table.
        Also stores the st id in results in other_data of the lookup table'''
        dbase= get_database(self.database)
        success=True
        try:
            #lookup will store the information received and also store
            #the number of jobs sent for this scheme/assembly combination
            lookup =  self.get_lookup()
            st_barcode=None
            st_id=None
            results=None
            if  isinstance(self.results['log'], str):
                raise Exception("Error getting data from log in the results, log: %s"%results['log'])

            st_barcode = self.results['log']['ST'][1]
            st_id= self.results['log']['ST'][0]
           
            
            #store the results
            lookup.st_barcode=st_barcode
            lookup.other_data['results']['st_id']=st_id
            
            #write out the gff file
            data_rows=[]
            data_rows.append("##gff-version 3")
            info=self.results['log']
            for key in info:
                #check it as a locus
                if type(info[key]) is list:
                    if type(info[key][0]) is dict:
                        locus_list = info[key]
                        for locus_info in locus_list:
                            row="%s\t.\tCDS\t%s\t%s\t.\t%s\t0\tID=%s;Name=%s"
                            row = row %(locus_info['contig'],locus_info['start'],locus_info['end'],locus_info['direct'],key,key)
                            data_rows.append(row)
            seqs = info.get('sequence')
            if seqs:
                data_rows.append("##FASTA")
                pos=1
                for seq in seqs:
                    data_rows.append(">"+str(pos))
                    data_rows.append(seq)
                    pos=pos+1
            text= "\n".join(data_rows)  
            lookup.annotation = text
            
        except Exception as e:
            app.logger.exception("Problems processing Nomenclature job %i and results %s" % (self.job_id, self.results))
            success=False
        self.job_processed(success,self.results['version'])
        return success



class GenericJob(CRobotJob,AssemblyBasedJob):
    '''Represents a generic job that stores the results in other_data ->results.
    Assumes that crobot job gets the file pointer from inputs->assembly.
    Any params can be specified and in addition any params specified in the scheme
    table are also added.
    '''
    def __init__(self,**kwargs):
        if kwargs.get("data"):
            job_db=kwargs["job_db"]
            dbase=get_database(job_db.database)
            Schemes = dbase.models.Schemes
            arr= job_db.pipeline.split(":")
            scheme_desc=arr[0]
            scheme = dbase.session.query(Schemes).filter(Schemes.description ==scheme_desc).one()
            self.scheme_id =scheme.id
            kwargs['assembly_barcode']=job_db.accession
            self.type=scheme.description
            DataParam = dbase.models.DataParam
            params =dbase.session.query(DataParam).filter_by(tabname=scheme_desc).all()
            self.fields={}
            data = kwargs['data']
            #load all the results into self.fields
            if data.get("log"):
                data['log']=ujson.loads(data['log'])
            for param in params:
                self.fields[param.name]=self. __get_field_value(data,param.mlst_field)

        else:
            scheme = kwargs['scheme']
            kwargs['pipeline']=scheme.param['pipeline']
            kwargs['tag']=kwargs['assembly_barcode']
            kwargs['inputs']={"assembly":kwargs['assembly_filepointer']}
            params={}
            if  not kwargs.get('params'):
                kwargs['params']={}
            self.type=scheme.description
            self.fields={}

        super(GenericJob,self).__init__(**kwargs)

    def process_job(self):
        success=True
        try:
            dbase=get_database(self.database)
            DataParam = dbase.models.DataParam
            lookup=self.get_lookup()
            lookup.st_barcode="EGG"
            for key in self.fields:
                lookup.other_data['results'][key]=self.fields[key]
        except Exception as e:
            app.logger.exception("Problem processing generic job %i" % self.job_id)
            success =False
        self.job_processed(success, self.results['version'])
        

    def __get_field_value(self,dic,path):
        arr= path.split(",")
        try:
            for field in arr:
                try:
                    field= int(field)
                except:
                    pass
                dic=dic[field]
            return dic
        except Exception as e:
            return "";

class CRISPRJob(CRobotJob,AssemblyBasedJob,):
    def __init__(self,**kwargs):
        super(CRISPRJob,self).__init__(**kwargs)
        self.type="CRISPR"
        self.pipeline="CRISPer"
        
        if not kwargs.get("data"):
            self.inputs={}
            self.params={"scheme":kwargs['scheme'].param['scheme'],
                                    "prefix":self.assembly_barcode
                                  }
            dbase = get_database(self.database)
            assembly = dbase.session.query(dbase.models.Assemblies).filter_by(barcode=self.assembly_barcode).first()
            self.traces=assembly.traces
            self.set_reads()
        else:
            data =kwargs['data']
            dbase=get_database(self.database)
            Schemes = dbase.models.Schemes
            scheme_name=data['params']['scheme']
            scheme = dbase.session.query(Schemes).filter_by(description="CRISPR").one()
            self.scheme_id =scheme.id
            self.assembly_barcode=data['params']['prefix']

        self.tag=self.assembly_barcode

    def process_job(self):
        #process CRISPR
        info =self.results['log']
        lookup =self.get_lookup()
        lookup.st_barcode=info["CRISPR"]['barcode']
        #construct gff entry
        data_rows=[]
        data_rows.append("##gff-version 3")
        for gff_lists in info.get('gff',[]):
            for row in gff_lists:
                data_rows.append("\t".join(map(str,row)))
        seqs = info.get('sequence')
        if seqs:
            data_rows.append("##FASTA")
            pos=1
            for seq in seqs:
                data_rows.append(">"+str(pos))
                data_rows.append(seq)
                pos=pos+1
        text= "\n".join(data_rows)  
        lookup.annotation = text
        #update
        version =self.results.get('version')
        self.job_processed(True,version)
        #check has crispol result and processs as necessary
        crispol_barcode = info['CRISPOL'].get("barcode")
        if crispol_barcode:
            dbase = get_database(self.database)
            scheme = dbase.session.query(dbase.models.Schemes).filter_by(description="CRISPOL").first()
            self.scheme_id=scheme.id
            lookup =self.get_lookup(force=True)
            lookup.st_barcode=crispol_barcode
            self.job_processed(True,version)





class RefMapperJob(CRobotJob):
    '''Used to send assembliies for snp analysis. The snps are stored
    as gff files with pointers in the assembly lookup table with the scheme being 
    snp_calls and the reference barcode at ST_barcode.If the job is to be used for
    an snp project, then the tag supplied should be project_name:project_id
    In this case when the job is processed, a :class:`refMapperMatrix` is
    created and sent
    '''
    def __init__(self,**kwargs):
        '''database,inputs and params are required, as is tag.. user_id,priority and
        workgroup are optional
        
        :param inputs: a dictionary of the following
            * prefix - the reference assembly barcode
        :param inputs: a dictionary of the following
            * queries -  a dictionary where assembly barcodes as keys
            and the values are the file pointers
            * reference - the reference file pointer
        :param tag:  this is the accession field in the jobs database.
            For anp projects it should be name:id
        '''
        super(RefMapperJob,self).__init__(**kwargs)
        arr=self.tag.split(":")
        self.analysis_id=None
        #check to see if this job is connected to an snp job
        if len(arr)>1:
            self.analysis_id=int(arr[1])
        self.pipeline="refMapper"
        self.type="refMapper"
    
    def process_job(self):
        success=True
        try:
            snp_project=get_analysis_object(self.analysis_id)
            outputs = self.results['outputs']['alignments']
            alignments = outputs[1:]
            queries={}
            barcode_list=[]
            reference = self.results['inputs']['reference']
            #get the gff file pointers
            for path in alignments:
                if path.startswith("*"):
                    continue
                name = os.path.split(path)[1]
                barcode = name.split(".")[1]
                queries[barcode]=path
                barcode_list.append(barcode)
            dbase = get_database(self.database)
            Assemblies = dbase.models.Assemblies
            Schemes =dbase.models.Schemes
            Lookup = dbase.models.AssemblyLookup 
            snp_scheme= dbase.session.query(Schemes.id).filter_by(description='snp_calls').first()
            #add the gff pointers to the lookup
            assemblies  = dbase.session.query(Assemblies).filter(Assemblies.barcode.in_(barcode_list)).all()
            for ass in assemblies:
                lookup = dbase.session.query(Lookup).filter_by(assembly_id =ass.id,scheme_id=snp_scheme.id,
                                                                                             st_barcode=snp_project.ref_barcode).first()
                if not lookup:
                    lookup = Lookup(assembly_id =ass.id,scheme_id=snp_scheme.id,
                                                st_barcode=snp_project.ref_barcode,version = self.results['version'])
                    dbase.session.add(lookup)
                    lookup.other_data= ujson.dumps({"file_pointer":queries[ass.barcode]})
            dbase.session.commit()
        except Exception as e:
            app.logger.exception("cannot process refMapper Job %i, error message: %s " %(self.job_id, e.message))
            dbase.rollback_close_session()
            success=False
        self.job_processed(success)
        return success
    
    def job_sent(self,success):
        #this is handled by the snp project
        pass

    def job_failed(self):
        if self.analysis_id:
            snp_project =get_analysis_object(self.analysis_id)
            if snp_project.data.get("ref_mapper")<>'failed':
                snp_project.send_job_failed_email()
            snp_project.data['failed']=True
            snp_project.data['ref_mapper']="failed"
            snp_project.update()

    def job_processed(self,success):
        if self.analysis_id:
            snp_project=get_analysis_object(self.analysis_id)
            if success:
                snp_project.data['ref_mapper']="complete"
                snp_project.send_ref_mapper_matrix_job()
            else:
                snp_project.data['ref_mapper']="processing_failed"
                snp_project.data['failed']="true"
                snp_project.send_job_failed_email()
                snp_project.update()


class RefMapperMatrixJob(CRobotJob):
    def __init__(self,**kwargs):
        super(RefMapperMatrixJob, self).__init__(**kwargs)
        arr = self.tag.split(":")
        self.analysis_id=None
        if len(arr)>1:
            self.analysis_id=arr[1]
        self.type='refMapperMatrix'
        self.pipeline="refMapper_matrix"

    def process_job(self):
        snp_project = get_analysis_object(self.analysis_id)
        matrix = self.results['outputs']['matrix'][1]
        snp_project.data['matrix']=matrix 
        snp_project.data['ref_mapper_matrix']="complete"
        snp_project.send_matrix_phylogeny_job()

    #dealt with in process_job
    def job_processed(self,sucess):
        pass
    
    #dealt with by the snp project
    def job_sent(self,sucess):
        pass

    def job_failed(self):
        snp_project=get_analysis_object(self.analysis_id)
        if snp_project.data.get("ref_mapper_matrix")<>"failed":
            snp_project.send_job_failed_email()
        snp_project.data['ref_mapper_matrix']="failed"
        snp_project.data['failed']="true"
        snp_project.update()
       

class MatrixPhylogenyJob(CRobotJob):
    def __init__(self,**kwargs):
        super(MatrixPhylogenyJob,self).__init__(**kwargs)
        arr = self.tag.split(":")
        self.analysis_id=None
        if len(arr)>1:
            self.analysis_id=arr[1]
        self.type='matrix_phylogeny'
        self.pipeline="matrix_phylogeny"

    def process_job(self):
        from entero.cell_app.tasks import process_snp_job
        try:
            if self.analysis_id:
                snp_project=get_analysis_object(self.analysis_id)
                snp_project.data['raxml_tree']=self.results['outputs']['outfiles'][1]
                snp_project.data['matrix_phylogeny']="complete"
                snp_project.update()
                #do the computaionally hard stuff -
                if app.config["USE_CELERY"]:
                    process_snp_job.apply_async(args=[self.analysis_id],queue="entero")
                else:
                    process_snp_job(self.analysis_id)
        except Exception as e:
            raise Exception("process_job failed, results are: %s, errro message: %s"%(self.results, e.message))

            
    #dealt with by snp_project
    def job_sent(self,complete):
        pass

    #dealt with in process job
    def job_processed(self):
        pass

    def job_failed(self):
        snp_project=get_analysis_object(self.analysis_id)
        if not snp_project:
            return
        if snp_project.data.get("matrix_phylogeny")<>"failed":
            snp_project.send_job_failed_email()
        snp_project.data['matrix_phylogeny']="failed"
        snp_project.data['failed']="true"
        snp_project.update()

class MSTreeJob(CRobotJob):

    def __init__(self,**kwargs):
        self.type='MSTrees'
        kwargs['pipeline']='MSTrees'
        super(MSTreeJob, self).__init__(**kwargs)
        self.analysis_id=None
        arr=self.tag.split(":")
        if len(arr)>1:
            self.analysis_id=int(arr[1])
        
    def process_job(self):
        arr = self.tag.split(":")
        ws = get_analysis_object(int(arr[1]))
        obj={}
        obj['nwk']=self.results['outputs']['Tree'][1]
        obj['complete']="true"
        obj['profile']=self.results['outputs']['Profile'][1]
        if  hasattr(ws, 'data') and ws.data.get("failed"):
            del ws.data['failed']
        ws.add_data(obj)

    #handled by the tree object
    def job_sent(self,success):
        pass
    
    def job_failed(self):
        ms=get_analysis_object(self.analysis_id)
        if not ms.data.get("complete"):
            ms.data['failed']="true"
        ms.update()

class LocusSearchJob(CRobotJob):

    def __init__(self,**kwargs):
        self.type='locus_search'
        kwargs['pipeline']='nomenclature'
        super(LocusSearchJob, self).__init__(**kwargs)
        self.analysis_id=int(self.tag)
        
    def job_sent(self,success):
        pass


    def job_failed(self):
        ms=get_analysis_object(self.analysis_id)
        if not ms.data.get("complete"):
            ms.data['failed']="true"
        ms.update()    

    def process_job(self):
        from entero.ExtraFuncs.workspace import get_analysis_object
        dbase=get_database(self.database)
        try:
            tag = self.job_db.accession
            ls= get_analysis_object(int(tag))
            folder = ls.get_folder()
            path = os.path.join(folder,'locus_search.gff')
            out_handle = open(path,"w")
            info = self.results['log']
            data_rows=[]
            data_rows.append("##gff-version 3") 
            loci=[]
            
            locus_filter=None
            master_scheme = ls.data.get("master_scheme")
            if master_scheme:
                locus_filter=set()
                DataParam= dbase.models.DataParam
                records = dbase.session.query(DataParam.name).filter_by(tabname=ls.data['scheme'],group_name='Locus').all()
                for record in records:
                    locus_filter.add(record.name)
    
            for key in info:
                #check it as a locus
                if key == "ST":
                    continue
                if type(info[key]) is list:
                    if type(info[key][0]) is dict:
                        locus_list = info[key]
                        for locus_info in locus_list:
                            if locus_filter and not key in locus_filter:
                                continue
                            if not locus_info.get('sequence'):
                                continue
                            if not locus_info.get('status'):
                                locus_info['status']="present"
                            row="%s\t.\tCDS\t%s\t%s\t.\t%s\t0\tID=%s;Name=%s;status=%s;allele_id=%s"
                            row = row %(locus_info['contig'],locus_info['start'],locus_info['end'],locus_info['direct'],key,key,locus_info['status'],locus_info["id"])
                            data_rows.append(row)
                        loci.append(key)
    
            text="\n".join(data_rows)
            out_handle.write(text)
            out_handle.close()
            ls.data['gff_file']=path
            ls.data['loci']=loci
            ls.data['complete']='true'
            ls.update()
        except Exception as e:
            app.logger.exception("Processing Locus Search id %i , job id %i failed" % (self.analysis_id,self.job_id))
            dbase.rollback_close_session()
            self.job_failed()
        

class WholeAssemblyDownloadJob(CRobotJob):
    def __init__(self,**kwargs):
        super(WholeAssemblyDownloadJob,self).__init__(**kwargs)
        self.type = "genome_download"
        self.pipeline="genome_download"
        #the assemblies already created have been tagged with prospective strains ids
        #tag is not really required
        self.tag="genome_download"

    def process_job(self):
        dbase = get_database(self.database)
        Assemblies = dbase.models.Assemblies
        Strains = dbase.models.Strains
        Traces =dbase.models.Traces
        try:
            bar_to_pointer={}
        
            bar_to_acc= self.results['params']['acc'];
            for entry in self.results['outputs']['genome']:
                if entry.startswith("*"):
                    continue
        
                try:
                    name = os.path.split(entry)[1]
                    barcode = name.split("_gen")[0]
                    bar_to_pointer[barcode]=entry
                except:
                    app.logger.info("Unable to process:" % entry)
        
            bar_list=[]
            for barcode in bar_to_acc:
                bar_list.append(barcode)
            assemblies = dbase.session.query(Assemblies).filter(Assemblies.barcode.in_(bar_list)).all()
            asm_acc = {}
            for ass in assemblies :
                info = ujson.loads(ass.other_data)
                asm_acc[ bar_to_acc[ass.barcode] ] = [ass, info.get('strain_id', None), info.get('status', None), None]
            for trace in dbase.session.query(Traces).filter(Traces.accession.in_(list(asm_acc.keys()))) :
                if asm_acc[trace.accession][3] == None :
                    asm_acc[trace.accession][3] = trace

            for aid, (acc, (ass, strain_id, status, _)) in enumerate([ [k, v] for k, v in asm_acc.items() if v[3] == None ]) :
                trace = Traces(strain_id=int(strain_id), status=status, seq_platform=status, accession=acc)
                ass.traces.append(trace)
                dbase.session.add(trace)                
                if aid % 500 == 499 :
                    dbase.session.commit()
            dbase.session.commit()
            for trace in dbase.session.query(Traces).filter(Traces.accession.in_(list(asm_acc.keys()))) :
                if asm_acc[trace.accession][3] == None :
                    asm_acc[trace.accession][3] = trace
            strains = { v[1]:k for k, v in asm_acc.items() }
            for strain in dbase.session.query(Strains).filter(Strains.id.in_(list(strains.keys()))) :
                strains[strain.id] = strain
            
            for aid, (k, (ass, strain_id, _, trace)) in enumerate(asm_acc.items()) :
            #for aid, ass in enumerate(assemblies):
            #    info = ujson.loads(ass.other_data)
            #    strain = dbase.session.query(Strains).filter_by(id=int(info['strain_id'])).first()
            #    trace = dbase.session.query(Traces).filter_by(accession=bar_to_acc[ass.barcode]).first()
            #    if  not trace:    
            #        trace = Traces(strain_id=strain.id,status=info['status'],seq_platform=info['status'],accession=bar_to_acc[ass.barcode])
            #        ass.traces.append(trace)
            #        dbase.session.add(trace)
            #        dbase.session.commit()
                strain = strains[strain_id]
                ass_id = ass.id
                strain.best_assembly = ass_id
        
                trace.barcode = dbase.encode(trace.id, self.database, 'traces')
                file_pointer = bar_to_pointer.get(ass.barcode)
                if file_pointer:
                    ass.status='Assembled'
                    ass.file_pointer = file_pointer
                if aid % 500 == 0 :
                    dbase.session.commit()
            dbase.session.commit()
        except Exception as e:
            dbase.rollback_close_session()
            app.logger.exception("Error in processing job genome download job %i in database, error message: %s" % (self.job_id, e.message))

    
    def job_sent(self,success):
        pass

    def job_failed(self):
        pass

class RetrieveProfilesUser(CRobotJob):
    def __init__(self,**kwargs):
        super(RetrieveProfilesUser,self).__init__(**kwargs)
        #if not supplied with returned data need to build the job from the parameters supplied
        if  not kwargs.get("data"):
            download_name= str(kwargs['download_name'])
            scheme_id =kwargs['param']
            strain_ids=kwargs('strain_ids')

            #get strain names and associated st barcodes and put them in a map of st barcodes to strain names/ids
            sids = ",".join.map(str,strain_ids)
            sql ="SELECT ST_barcode AS st_bar, strain AS name, strains.id AS sid,assembly_lookup.other_data-- FROM strains INNER JOIN assemblies ON strains.best_assembly=assemblies.id INNER JOIN assembly_lookup ON scheme_id=%i AND assembly_id=assemblies.id WHERE strains.id IN (%s) " %  (scheme_id,sids)
            results = get_database(self.database).execute_query(sql)
            mappings= {}
            for item in results:
                li = mappings.get(item['st_bar'])
                if not li:
                    li=[]
                    mappings[item['st_bar']]=li
                li.append({"id":item['sid'],"name":item['name']})
            #add a list of all barcodes to the jobs params
            self.params["barcodes"]=mappings.keys()

            #get the database record that holds details about the profiile file 
            record = db.session.query(UserPreferences).filter_by(user_id=self.user_id,database=self.database,type="profile_download",name=download_name).first()
            if not record:
                record = UserPreferences(user_id=self.user_id,database=self.database,type="profile_download",name=download_name)
                db.session.add(record)
                db.session.commit()
            #create an analyis object for easier manipulation
            ao = Analysis(rec=record)
            #save the file and store pointer to it
            folder = ao.get_folder()
            file_name=os.path.join(folder,"barcode_mappings.json")
            out_handle = open(file_name,"w")
            out_handle.write(ujson.dumps(mappings))
            ao.data['mappings_file']=file_name
            ao.update()
            #what is record_id, it will crash onece this class called
            # I have chenged the attribute from record_id to record.id
            # it seems that it meant to be like that but not sure
            self.tag=str(record.id)
            self.analysis_object=ao
        else:
            #job has been recreated from data, get the database record from the tag
            record= db.session.query(UserPreferences).filter_by(id =int(self.tag))
            self.analysis_object=Analysis(rec=record)
        #constants for this object
        self.pipeline= "retrieve_profiles"
        self.type =  "retrieve_profiles_user"
      
        

    def job_sent(self,sucess):
        #job cannot be sent - update record
        if not sucess:
            self.analysis_object.data['failed']=True
            if self.analysis_object['complete']:
                del self.analysys_object['complete']
        #job sent ok - store job_id
        else:
            self.analysis_object.data['job_id']=self.job_id
        self.analysis_object.update()

    def job_failed(self):
        #need to update the user record
        self.analysis_object.data['failed']=True
        if self.analysis_object['complete']:
            del self.analysys_object['complete']
        self.analysis_object.update()


    def process_job(self):
        #need to rewrite file and map the barcodes to strain name(s)
        mapping_file =open(self.analysy_object.data['mappings_file'])
        mapping = ujson.loads(mapping_file.read())
        mapping_file.close()
        with open(self.results['outputs']['DMSTree'][1]) as f:
            for line in f:
                pass
        

@celery.task
def send_to_celery(args):
    job = network_job_classes[args['pipeline']]()
    for key in args:
        setattr(job,key,args[key])
    job.process_job()

def get_crobot_job(job_id=None,data=None):
    '''This method will try and create a job based on its id or the data returned from CRobot
    
    :param job_id: The id of the job (In which case CRobot will be contacted to get 
        params and inputs)
    :param data: The data received from CRobot (which contains the id )
    
    Returns the correct job object or None if it cannot be created 
    '''
    #get the data associated with this job
    try:
        if job_id:
            URI = app.config['CROBOT_URI']+"/head/show_jobs"
            params = "tag IN (%s)" % str(job_id)
            resp = requests.post(URI,data={"FILTER":params}, timeout=60)
            data = ujson.loads(resp.text)[0]

        job_class= data['pipeline']
        job_db=None
        #see if it is a crobot job as sometimes the job class is not the pipeline
        #e.g locus_search is a nomenclature job
        if data.get("tag"):
            job_db= db.session.query(UserJobs).filter_by(id=int(data['tag'])).one()
            #sometimes the job pipeline has extra information which needs to be stripped
            #eg nomenclature:scheme_name
            arr= job_db.pipeline.split(":")
            job_class=arr[0]
        else:
            app.logger.error("get_crobot_job failed, error message: tag value in the data (%s) does not have proper format"%data)
            return None
    except NoResultFound as e:
        app.logger.error("get_crobot_job failed,no resuls found for %s, error message: %s"%(data.get('tag'), e.message))
        rollback_close_system_db_session()
        return None

    except Exception as e:
        app.logger.exception("get_crobot_job failed, error message: %s"%e.message)
        rollback_close_system_db_session()
        return None

    return network_job_classes[job_class](data=data,job_db=job_db)

def send_all_jobs_for_assembly(assembly,database,user_id=0,priority=0,workgroup="user_upload"):
    dbase=get_database(database)
    scheme_info = dbase.session.query(dbase.models.Schemes).all()
    for scheme in scheme_info:
        #Take CRISPer pipeline out as Zhemin has mentioned that we stopped running it permanently and we need to take it out
        #K.M 30/05/2019
        if scheme.name=='CRISPR':
            continue
        if scheme.name=='Serotype Prediction (SISTR0)' and database=='senterica':
            continue 
        pipeline = scheme.param.get('pipeline')
        if not pipeline:
            continue
        job_rate = scheme.param.get("job_rate")
        if job_rate==0:
            continue
        key = pipeline
        if key <> 'nomenclature':
            key = scheme.description
        job = network_job_classes[key](scheme=scheme,
                                            assembly_filepointer=assembly.file_pointer,
                                            assembly_barcode=assembly.barcode,
                                            database=database,
                                            user_id=user_id,
                                            priority=priority,
                                            workgroup=workgroup
                                            )
        job.send_job()    
    



network_job_classes={}
network_job_classes['locus_search']= getattr(sys.modules[__name__],"LocusSearchJob")
network_job_classes['MSTrees']=getattr(sys.modules[__name__],"MSTreeJob")
network_job_classes['nomenclature']=getattr(sys.modules[__name__],"NomenclatureJob")
network_job_classes['SeroPred']=getattr(sys.modules[__name__],"GenericJob")
network_job_classes['SeroPred_2']=getattr(sys.modules[__name__],"GenericJob")
network_job_classes['AlternType']=getattr(sys.modules[__name__],"GenericJob")
network_job_classes['prokka_annotation']=getattr(sys.modules[__name__],"GenericJob")
network_job_classes['QAssembly']=getattr(sys.modules[__name__],"AssemblyJob")
network_job_classes['refMasker']=getattr(sys.modules[__name__],"RefMaskerJob")
network_job_classes['refMapper']=getattr(sys.modules[__name__],"RefMapperJob")
network_job_classes['refMapperMatrix']=getattr(sys.modules[__name__],"RefMapperMatrixJob")
network_job_classes['matrix_phylogeny']=getattr(sys.modules[__name__],"MatrixPhylogenyJob")
network_job_classes['genome_download']=getattr(sys.modules[__name__],"WholeAssemblyDownloadJob")
network_job_classes['CRISPR']=getattr(sys.modules[__name__],"CRISPRJob")
network_job_classes['QA_evaluation']=getattr(sys.modules[__name__],"QA_evaluation_Job")
import entero.ExtraFuncs.temp

