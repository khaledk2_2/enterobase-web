# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
# from entero.databases.system.models import User

from entero.test_utilities import ebHTTPNotAuth, get_api_headers, enHashInfoQueryResponseData

from urllib2 import HTTPError
import urllib2
import json
import re

@pytest.mark.slow
class TestStrainsversionQueryCase1(object):
    strainsversion_query1 = "/api/v2.0/senterica/strainsversion?strainsversion?strain_name=RM_172"
  
    @pytest.fixture(scope='class') 
    def strainsversion_query1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.strainsversion_query1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def strainsversion_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.strainsversion_query1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def strainsversion_query1_jack_auth_response_data(self, strainsversion_query1_jack_auth_response):
        return json.loads(strainsversion_query1_jack_auth_response.get_data(as_text=True))

    def test_strainsversion_query1_bad_auth_status(self, strainsversion_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert strainsversion_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert strainsversion_query1_bad_auth_response.status_code == 401

    def test_strainsversion_query1_jack_auth_status(self, strainsversion_query1_jack_auth_response):
        assert strainsversion_query1_jack_auth_response.status_code == 200

    def test_strainsversion_query1_non_empty(self, strainsversion_query1_jack_auth_response_data):
        assert strainsversion_query1_jack_auth_response_data is not None
        assert len(strainsversion_query1_jack_auth_response_data) > 0

    def test_strainsversion_query1_has_strainsarchive_key(self, strainsversion_query1_jack_auth_response_data):
        assert "StrainsArchive" in strainsversion_query1_jack_auth_response_data

    def test_strainsversion_query1_has_links_key(self, strainsversion_query1_jack_auth_response_data):
        assert "links" in strainsversion_query1_jack_auth_response_data

    def test_strainsversion_query1_records_gt_0(self, strainsversion_query1_jack_auth_response_data):
        assert strainsversion_query1_jack_auth_response_data["links"]["records"] >= 1

class TestStrainsversionQueryCase2(object):
    strainsversion_query1 = "/api/v2.0/senterica/strainsversion?only_fields=city&only_fields=collection_year&only_fields=country&only_fields=lastmodified&only_fields=serotype&only_fields=source_details&only_fields=source_type&only_fields=strain_barcode&only_fields=strain_name&only_fields=version&strain_name=Ragna"
    onlyFieldsSet = set([re.sub("only_fields=", "", x) for x in strainsversion_query1.split("?")[1].split("&") if re.match("only_fields", x) is not None])
  
    @pytest.fixture(scope='class') 
    def strainsversion_query1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.strainsversion_query1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def strainsversion_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.strainsversion_query1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def strainsversion_query1_jack_auth_response_data(self, strainsversion_query1_jack_auth_response):
        return json.loads(strainsversion_query1_jack_auth_response.get_data(as_text=True))

    def test_strainsversion_query1_bad_auth_status(self, strainsversion_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert strainsversion_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert strainsversion_query1_bad_auth_response.status_code == 401

    def test_strainsversion_query1_jack_auth_status(self, strainsversion_query1_jack_auth_response):
        assert strainsversion_query1_jack_auth_response.status_code == 200

    def test_strainsversion_query1_non_empty(self, strainsversion_query1_jack_auth_response_data):
        assert strainsversion_query1_jack_auth_response_data is not None
        assert len(strainsversion_query1_jack_auth_response_data) > 0

    def test_strainsversion_query1_has_strainsarchive_key(self, strainsversion_query1_jack_auth_response_data):
        assert "StrainsArchive" in strainsversion_query1_jack_auth_response_data

    def test_strainsversion_query1_has_links_key(self, strainsversion_query1_jack_auth_response_data):
        assert "links" in strainsversion_query1_jack_auth_response_data

    def test_strainsversion_query1_records_gt_0(self, strainsversion_query1_jack_auth_response_data):
        assert strainsversion_query1_jack_auth_response_data["links"]["records"] >= 8

# TO DO - new test for only_fields in strainsversion query 2/ Ragna query
    def test_strainsversion_query1_only_has_only_fields(self, strainsversion_query1_jack_auth_response_data):
        for strainVersion in strainsversion_query1_jack_auth_response_data["StrainsArchive"]:
            currFieldsSet = set(strainVersion.keys())
            assert self.onlyFieldsSet == currFieldsSet
            
