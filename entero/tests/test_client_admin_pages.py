import pytest
# import flask
import ujson

from entero.config import Config
from entero.test_utilities import publicDBList, fullDBList, nonPublicDBList, jackDBList, nonJackDBList, testbossDBList, nonTestbossDBList, getATagURLList, fakeConfigActiveDBHash

# WVN 13/3/18 IMPORTANT
# Note that a lot of admin tests are expected to fail if the tests are run
# in the normal way with the testing config - since this skips loading up
# a lot of stuff required for the admin pages to work.  Indeed at the
# time of writing it's just been confirmed that the entire set of tests
# actually passes providing that the correct config is used.
# In order to run the admin tests, it is necessary to change the app
# fixture to create_app with "production" as an argument instead
# of "testing".

class TestAdminEndpointNotLoggedIn:
    @pytest.fixture(scope = "class")
    def endpoint_response(self, eb_client):
        return eb_client.get("/admin/")
    
    @pytest.fixture(scope = "class")
    def trunc_endpoint_response(self, eb_client):
        return eb_client.get("/admin")    

    # Behaviour is that we get an essentially empty page with just a link to the site home on the left hand side
    # But this happens via a redirect (so status code is 301).
    # Below method would crash rather than returning None because json.loads did not like to run on a None.
    @pytest.fixture(scope = "class")
    def endpoint_response_data(self, endpoint_response):
        return endpoint_response.get_data(as_text = True)
        # Not sure where I got the version of code below that invoked ujson.loads on the data but
        # it's wrong - keeping it below for now for debugging purposes
        # Martin said in e-mail to use ujson but
        # examples I have use json
    #    return ujson.loads(endpoint_response.get_data())
    #    # return json.loads(endpoint_response.get_data())

    def test_endpoint_response_status(self, endpoint_response):
        assert endpoint_response.status_code == 200
        
    def test_trunc_endpoint_response_status(self, trunc_endpoint_response):
        assert trunc_endpoint_response.status_code == 301    

    #def test_endpoint_response_get_data_is_none(self, endpoint_response_data):
    #    assert endpoint_response_data is None
    
    # Response is raw HTML and can't run ujson.loads() on it; but it is *not* None either    
    def test_endpoint_response_get_data_is_not_none(self, endpoint_response_data):
        assert endpoint_response_data is not None
        
    # Further tests to do on content and absence of it
    def test_endpoint_response_data_hasnt_admin_home_in_content(self, endpoint_response_data):
        assert "Admin Home" not in endpoint_response_data
        
    def test_endpoint_response_data_hasnt_system_in_content(self, endpoint_response_data):
        assert "System" not in endpoint_response_data    

    @pytest.mark.parametrize("db_name", fullDBList)    
    def test_endpoint_response_data_hasnt_readable_db_name_in_content(self, endpoint_response_data, db_name):
        readableDBName = fakeConfigActiveDBHash[db_name][0]
        assert readableDBName not in endpoint_response_data  
    
class TestAdminEndpointJackLoggedIn:
    @pytest.fixture(scope = "class")
    def endpoint_response(self, eb_logged_in_client):
        return eb_logged_in_client.get("/admin/")
    
    @pytest.fixture(scope = "class")
    def trunc_endpoint_response(self, eb_client):
        return eb_client.get("/admin")     

    @pytest.fixture(scope = "class")
    def endpoint_response_data(self, endpoint_response):
        # Note that we get back raw HTML here not JSON
        return endpoint_response.get_data(as_text = True)

    def test_endpoint_response_status(self, endpoint_response):
        assert endpoint_response.status_code == 200
        
    def test_trunc_endpoint_response_status(self, trunc_endpoint_response):
        assert trunc_endpoint_response.status_code == 301     

    def test_endpoint_response_get_data_is_not_none(self, endpoint_response_data):
        assert endpoint_response_data is not None
            
    # Further tests to do on content and absence of it
    def test_endpoint_response_data_hasnt_admin_home_in_content(self, endpoint_response_data):
        assert "Admin Home" not in endpoint_response_data
            
    def test_endpoint_response_data_hasnt_system_in_content(self, endpoint_response_data):
        assert "System" not in endpoint_response_data    
    
    @pytest.mark.parametrize("db_name", fullDBList)    
    def test_endpoint_response_data_hasnt_readable_db_name_in_content(self, endpoint_response_data, db_name):
        readableDBName = fakeConfigActiveDBHash[db_name][0]
        assert readableDBName not in endpoint_response_data     
        
class TestAdminEndpointTestbossLoggedIn:
    @pytest.fixture(scope = "class")
    def endpoint_response(self, eb_testboss_logged_in_client):
        return eb_testboss_logged_in_client.get("/admin/")
    
    @pytest.fixture(scope = "class")
    def trunc_endpoint_response(self, eb_client):
        return eb_client.get("/admin")     

    @pytest.fixture(scope = "class")
    def endpoint_response_data(self, endpoint_response):
        return endpoint_response.get_data(as_text = True)

    def test_endpoint_response_status(self, endpoint_response):
        assert endpoint_response.status_code == 200
        
    def test_trunc_endpoint_response_status(self, trunc_endpoint_response):
        assert trunc_endpoint_response.status_code == 301 

    def test_endpoint_response_data_not_none(self, endpoint_response_data):
        assert endpoint_response_data is not None

    def test_endpoint_response_data_has_admin_home_in_content(self, endpoint_response_data):
        assert "Admin Home" in endpoint_response_data
        
        def test_endpoint_response_data_has_system_in_content(self, endpoint_response_data):
            assert "System" in endpoint_response_data    

    @pytest.mark.skip(reason = "This content no longer appearing in sidebar for EnteroBase - broken or not?")
    @pytest.mark.parametrize("db_name", testbossDBList)    
    def test_endpoint_response_data_has_testboss_readable_db_name_in_content(self, endpoint_response_data, db_name):
        readableDBName = fakeConfigActiveDBHash[db_name][0]
        assert readableDBName in endpoint_response_data
