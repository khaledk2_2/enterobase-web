#https://github.com/authlib/example-oauth2-server/tree/master/website
#https://github.com/authlib/example-oauth2-server/tree/master/website

from entero import db
from ..databases.system.models import LocalClient, UserGrantAuthorizationCode, UsersToken, User, LocalClientUsersConfirmation, LocalClientTestsResults
from authlib.flask.oauth2 import AuthorizationServer, ResourceProtector
from authlib.flask.oauth2.sqla import (
    create_query_client_func,
    create_save_token_func,
    create_revocation_endpoint,
    create_bearer_token_validator,
)

from authlib.oauth2.rfc6749 import grants
from werkzeug.security import gen_salt

class AuthorizationCodeGrant(grants.AuthorizationCodeGrant):
    def create_authorization_code(self, client, grant_user, request):
        code = gen_salt(48)
        item = UserGrantAuthorizationCode(
            code=code,
            client_id=client.client_id,
            redirect_uri=request.redirect_uri,
            scope=request.scope,
            user_id=grant_user.id,
        )
        db.session.add(item)
        db.session.commit()
        return code

    def parse_authorization_code(self, code, client):
        item = UserGrantAuthorizationCode.query.filter_by(
            code=code, client_id=client.client_id).first()
        if item and not item.is_expired():
            return item

    def delete_authorization_code(self, authorization_code):
        db.session.delete(authorization_code)
        db.session.commit()

    def authenticate_user(self, authorization_code):
        return User.query.get(authorization_code.user_id)


class PasswordGrant(grants.ResourceOwnerPasswordCredentialsGrant):
    def authenticate_user(self, username, password):
        user = User.query.filter_by(username=username).first()
        if user is not None and user.check_password(password):
            return user


class RefreshTokenGrant(grants.RefreshTokenGrant):
    def authenticate_refresh_token(self, refresh_token):
        token = UsersToken.query.filter_by(refresh_token=refresh_token).first()
        if token and token.is_refresh_token_active():
            return token

    def authenticate_user(self, credential):
        return User.query.get(credential.user_id)

    def revoke_old_credential(self, credential):
        credential.revoked = True
        db.session.add(credential)
        db.session.commit()


query_client = create_query_client_func(db.session, LocalClient)
save_token = create_save_token_func(db.session, UsersToken)
authorization = AuthorizationServer(
    query_client=query_client,
    save_token=save_token,
)
def finc(request):
    return True
#authorization.create_oauth2_request=finc

require_oauth = ResourceProtector()

def config_oauth(app):
    authorization.init_app(app)
    # support all grants
    authorization.register_grant(grants.ImplicitGrant)
    authorization.register_grant(grants.ClientCredentialsGrant)
    authorization.register_grant(AuthorizationCodeGrant)
    authorization.register_grant(PasswordGrant)
    authorization.register_grant(RefreshTokenGrant)

    # support revocation
    revocation_cls = create_revocation_endpoint(db.session, UsersToken)
    authorization.register_endpoint(revocation_cls)

    # protect resource
    bearer_cls = create_bearer_token_validator(db.session, UsersToken)
    require_oauth.register_token_validator(bearer_cls())

    try:
        db.create_all()
    except:
        pass

def save_user_confirmation(client_id,user_id):
    '''
    save the user confirmation
    :param user_id:
    :param client_id:
    :return:
    '''
    #first check if there is a row in the table
    con_users=db.session.query(LocalClientUsersConfirmation).filter(LocalClientUsersConfirmation.user_id==user_id).filter(LocalClientUsersConfirmation.client_id==client_id).all()
    if len(con_users)>0:
       con_users[0].active=True
    else:
        con_user=LocalClientUsersConfirmation()
        con_user.user_id=user_id
        con_user.client_id = client_id
        db.session.add(con_user)
    db.session.commit()


def check_user_confirmation(client_id, user_id):
    '''
    check if the user has confirmed and authorize client
    '''
    con_users=db.session.query(LocalClientUsersConfirmation).filter(LocalClientUsersConfirmation.user_id==user_id).filter(LocalClientUsersConfirmation.client_id==client_id).all()
    #if no filed is found in the table, the user needs to confirm authorization
    if len(con_users)==0:
        return False
    #return active field in the table
    #True if the user is not revoked his permission
    return con_users[0].active

    #LocalClientUsersConfirmation

def check_client_registration(client_id, client_secret, redirect_uri):
    '''
    check if the local client is registered with global enterobase
    :param client_id:
    :param client_secret:
    :param redirect_uri:
    :return:
    '''
    client = db.session.query(LocalClient).filter(LocalClient.client_id==client_id).filter(LocalClient.client_secret==client_secret).filter(LocalClient.redirect_uri==redirect_uri).all()
    if len(client)>0:
        return "True"
    else:
        return "False"

def check_client(client_id, user_id=None):
    if not client_id:
        return "False"
    client = db.session.query(LocalClient).filter(LocalClient.client_id == client_id).all()
    print len(client)
    if len(client) > 0:
        if user_id:
            #when user id is provided, it will check if the user is local adminstrator or nor
            if client[0].user_id==user_id:
                return True
            else:
                return False
        return "True"
    else:
        return "False"

def save_upload_test_results_to_database(client_id, no_test_uploaded_files,ave_uploaded_time):
    localClientTestsResults=db.session.query(LocalClientTestsResults).filter(LocalClientTestsResults.client_id==client_id).all()
    if len(localClientTestsResults)==0:
        localClientTestsResult=LocalClientTestsResults()
        localClientTestsResult.client_id=client_id
        localClientTestsResult.ave_uploaded_time=ave_uploaded_time
        localClientTestsResult.no_test_uploaded_files=no_test_uploaded_files
        db.session.add(localClientTestsResult)
    else:
        localClientTestsResult=localClientTestsResults[0]
        localClientTestsResult.ave_uploaded_time = ave_uploaded_time
        localClientTestsResult.no_test_uploaded_files = no_test_uploaded_files
    db.session.commit()
    return "True"

