from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, url
from wtforms.widgets import TextArea

class Create_Client_Form(FlaskForm):
    client_name = StringField('Client Name', validators=[DataRequired()], render_kw={"cols": "32"})
    client_uri = StringField('Client URL', validators=[DataRequired(), url()], render_kw={"cols": "32"})
    description = StringField('Client Description',widget=TextArea(), render_kw={"rows": "3", "cols": "32"})
    #redirect_uri = StringField('Redirect URIs',widget=TextArea(), validators=[DataRequired(), url()], render_kw={"rows": "2", "cols": "32"})
    submit = SubmitField('Submit')
