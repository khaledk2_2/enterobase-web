Dear {{ user.firstname }},

Your SNP Project has finished, you can load it from Enterobase or paste the following link in your browser

{{url}}

Sincerely,

The Enterobase Team
