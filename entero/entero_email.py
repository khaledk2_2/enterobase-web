from threading import Thread
from entero import app
from flask import current_app, render_template
#from entero import mail
import sys
#import zmq
import json
import smtplib
import mimetypes
import email
from email import Encoders
from email.Utils import formatdate,make_msgid
from email.Message import Message
from email.MIMEAudio import MIMEAudio
from email.MIMEBase import MIMEBase
from email.MIMEMultipart import MIMEMultipart
from email.MIMEImage import MIMEImage
from email.MIMEText import MIMEText
from email.MIMEMessage import MIMEMessage

#

def log_message(message, app):
    app.logger.info(message.subject)



def send_async_email(app, msg,to):
    app.logger.info("Sending email to "+ to)
    with app.app_context():
        try: 
            mail.send(msg)
            app.logger.info("Email to "+to +" sent")
            #email_dispatched.connect(log_message)
        except Exception as e:
            pass
            app.logger.exception("Email to "+to+" could not be sent")

def send_email(to, subject, template, **kwargs):
  
    sender=app.config['ENTERO_MAIL_SENDER']
    subject = app.config['ENTERO_MAIL_SUBJECT_PREFIX'] + ' ' + subject
    #msg = Message(subject, sender=sender, recipients=[to])
    body = render_template(template + '.txt', **kwargs)
    #msg.html = render_template(template + '.html', **kwargs)
    #sendmail(me=sender,  to=[person], subject=subject,txt=msg)
    thr = Thread(target=sendmail, kwargs=dict(me=sender, to = [to], subject=subject,txt=body))
       
    thr.start()  
    
def sendmail(filelist=[],me = 'enterobase@warwick.ac.uk',to=['enterobase@warwick.ac.uk'],subject = 'Submitting new alleles and STs to MLST and EnteroBase',txt=''):

    COMMASPACE = ', '
    if not isinstance(to, list):
        to = [to]
    outer = MIMEMultipart()
    outer['Subject'] = subject
    outer['From'] = me
    outer['To'] = COMMASPACE.join(to)
    #outer['Cc'] = me
    outer['Date'] = formatdate(localtime=1)
    outer['Message-ID'] = make_msgid()
    outer.preamble = 'To Enterobase user'
    outer.epilogue = ''
    msg = MIMEText(txt.encode('utf_8'))
    outer.attach(msg)
    Esau = app.config['MAIL_SERVER']#'137.205.123.28'
    try:
        s = smtplib.SMTP()
        s.connect(Esau)
        s.sendmail(me, to, outer.as_string())
        s.close()
    except Exception as e:
        app.logger.exception("email failed, error message: %s"%e.message)
    
    
    
'''def sendZMQEmail(sender='zhemin.zhou@warwick.ac.uk', to=['m.j.sergeant@warwick.ac.uk'],
             subject = 'zeromq_email test', content_text = 'test mail final\n', content_html = 'test <b>mail</b> finalx\n'):
    context = zmq.Context()
    socket = context.socket(zmq.PUSH)

    socket.connect("tcp://%s:4999" % app.config["POSTFIX_SERVER_ADDRESS"]) 
    
    mail_param = {'From' : sender,
                  'To' : to, 
                  'Subject' : subject,
                  'Content_text' : content_text,
                  'Content_html' : content_html}
    cmd = json.dumps(mail_param) 
    socket.send(cmd.encode('utf-8'))
    print(cmd)'''
