.. enterobase documentation master file, created by
   sphinx-quickstart on Fri Mar  3 18:54:07 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to enterobase's documentation!
======================================

.. toctree::
   :maxdepth: 3

   source/entero.ExtraFuncs
   source/entero.jobs
   source/entero.species
   source/entero.jbrowse

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
