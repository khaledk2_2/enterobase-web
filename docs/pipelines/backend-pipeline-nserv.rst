NServ
=====
NServ is a standalone server that can handle all the requests related to
MLST, cgMLST and other MLST schemes, across all the taxa handled in
EnteroBase. It offers sets of APIs to both communicate with users and
bioinformatic tools, and synchronize MLST data with existing MLST
websites (currently only MLST UoW).

NServ is still under development. The current tool for MLST typing is a
task named "[nomenclature]" implemented in EnteroTools.

API
---
NServ URI
^^^^^^^^^
In the examples below, the NServ URI is configuration dependent, depending on
which system NServ runs.

Query methods (walking through database structure)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Database query**

In order to get a list of all current databases make an HTTP GET request
using the URL

``http://<NServ Host>/ET/NServ/query.api``

**Table query**
In order to get all available tables in a database, for example the
Salmonella database use the URL

``http://<NServ Host>/ET/NServ/query.api/Salmonella``

**Index/ within-table query**
To get all indexes in a table, for example the alleles table in the
Salmonella database use the URL

``http://<NServ Host>/ET/NServ/query.api/Salmonella/alleles``

**Record/ query lookup**
In order to get information for a specific record, for example the record
with index 100 in the
alleles table of the Salmonella database

``http://<NServ Host>/ET/NServ/query.api/Salmonella/alleles/100``

Retrieve methods (Find information with barcodes
------------------------------------------------

**Barcode lookup**

In order to get information directly from a barcode, for example the barcode
SAL_AA0100AA_ST use the URL

``http://<NServ Host>/ET/NServ/retrieve.api/SAL_AA0100AA_ST>``  
( Alternatively, you can POST lots of barcodes at one batch in this
format:  
``http://<NServ Host>/ET/NServ/retrieve.api?barcode=SAL_AA0002AA_LO``

**Lookup of historical archives for a barcode**
For example, in order to get all historical archives for the barcode
SAL_AA0002AA_LO use the URL

``http://<NServ Host>/ET/NServ/retrieve.api/SAL_AA0002AA_LO/archives``

\#Currently no history

**Lookup of a historical version for a barcode**
For example, in order to get the historical version 1 for the barcode
SAL_AA0002AA_LO use the URL

``http://<NServ Host>/ET/NServ/retrieve.api/SAL_AA0002AA_LO/archives?version=1``

\#Currently no history

Dedicated search methods (batch downloads or specified queries)
---------------------------------------------------------------

**Search for alleles for a given locus**

For example, in order to download all alleles for locus aroC in Salmonella MLST scheme use

1.  ``http://<NServ Host>/ET/NServ/search.api/Salmonella/UoW/alleles?filter=fieldname%20in%20('aroC')``  
    or  
2.  ``http://<NServ Host>/ET//NServ/search.api/Salmonella/UoW/alleles?locus=aroC``  
    Both the database name and the table name are required.

**More complex queries for alleles**

The searching criteria can be submitted with either filter=<SQL
statement>, or key=value pairs, or a combination of these two ways.
For example, downloading only the first five alleles:

1.  ``http://<NServ Host>/ET/NServ/search.api/Salmonella/UoW/alleles?locus=aroC&filter=allele_id between 1 and 5``  
    search.api also supports complex search terms like :
2.  ``http://<NServ Host>/ET/NServ/search.api/Salmonella/UoW/alleles?locus=aroC&allele_id=between 1 and 5``

**Search for allele with a specific sequence**

An example of a search for a known allele id - in the Salmonella database with
the exact sequence (see below) is given by the URL

``http://<NServ Host>/ET/NServ/search.api/Salmonella/UoW/alleles?locus=aroC&seq=GTTTTTCGCCCGGGACACGCGGATTACACCTATGAGCAGAAATACGGCCT``

**Matching methods (finds alleles with sequence, or finds STs with alleles)**

``match.api`` queries not documented yet...

  [nomenclature]: EnteroBase%20Backend%20Pipeline%3A%20nomenclature%20combined