MS Trees
========

MS Trees are stored in the user_preferences table of the entero (system)
database, type being ms_tree or ms_sa_tree (stand alone MS tree not linked
with a workspace). The data column contains the following json

* **data_file**  The location of the file containing json data in the following format
    * **scheme_genes** obsolete - A list of all the loci used for the scheme (can be retrieved from the scheme)
    * **scheme** The name (description) of the scheme
    * **isolates** A list of the metadata for each strain. Each item in the list consists of a dictionary of  key/value pairs, with 'StrainID' being the strain id in the database and ID being the node ID that the strain is associated with e.g. {"StrainID":5641,"ID":"ST454","Name":"bug1","Country":"Germany",'''''}
    * **links** A list of the links in the tree, each item is a dictionary with:-
        * **source** The index (in the identifiers) of the source of the link
        * **target** The index of the link target
        * **distance** The relative length of the link
    * **identifiers** a list of all the node identifiers. Each item is another list of all the STs in the node (due to missing data, a node can occasionally consist of more than one ST) e.g. [["ST3453","ST232"],["ST454"],["ST23"],.....]
    * **metadata_options** A dictionary of categories to field name:labels  e.g {"Metadata{"city":City",...},"Custom Fields:{"field_a":"Field A"}}
    * **current_metadata** The name of the metadata last displayed in the tree

* **layout_file** The location of the file containing the layout information of the tree
    * **node_positions** A dictionary of node id to an array of x,y co-ordinates e.g {"ST232":[762,232],"ST233":[22,33],...}
    * **nodes_links** Extra information about the nodes/links:
        * **max_link_scale**
        * **base_node_size**
        * **size_power**
        * **scale**
        * **translate**

* **parent** The id of the parent workspace (if applicable)
* **complete** 'true' if the tree has been calculated
