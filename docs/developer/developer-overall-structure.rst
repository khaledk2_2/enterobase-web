Overall Directory Structure
=================
entero
^^^^^^
The main python package containing all the python code.

**logs**

Contains the log files. Info log contains general information e.g when an
assembly was sent or when a user uploaded a read, whereas the error log
contains information about all the exceptions thrown. The system log contains
information from the any scripts that are run (see below)

**scripts**

The folder contains shell scripts, which should be run every so often (e.g.
using crontab) to update and maintain the database. The two scripts run most
regularly are:-

* *daily_update.sh* Checks databases to see if any assemblies need to be sent or any  schemes need to be called. Simply calls [update_all_schemes](Maintenance Scripts#markdown-header-update_all_schemes) and [update_assemblies](Maintenance Scripts#markdown-header-update_assemblies) for the databases specified in the script. The script can be changed to add/remove databases, change priorities,job queues etc.
* *daily_import.sh* Updates all databases by calling [importSRA](Maintenance Scripts#markdown-header-importsra)

**js_docs**

The html sub directory contains the actual JavaScript documentation produced
from the inline code comments. Just open the index.html in this folder with a
browser to view the documentation. In order to update documentation jsdoc
needs to be installed (*npm install jsdoc*). Also a bootstrap template is
used (*npm install ink-docstrap*). In the config.json file change the
template in opts to the correct location (depending on where ink-docstrap was
installed).

.. code-block:: json

    "opts":{
        "destination":"html",
        "readme":"README.rst",
        "template":"/usr/local/lib/node_modules/ink-docstrap/template"
    }


If you want to use the default template, then just delete the template entry
in the config. Any new .js files need to be added to the include entry in the
config file.

.. code-block:: json

    "source": {
        "include":
            [
             "../entero/static/js/tree/d3_m_tree.js",
             "../entero/static/js/table/validation_grid.js",
              ............								                  
            ]
    }


To produce the documentation just cd to the *jsdoc directory* and run 
.. code-block:: json

    jsdoc -c conf.json


**python_docs**

The actual documentation is on the *_build/html* subdirectory and can be
viewed by opening index.html in a browser. To update the documentation cd to
the python_docs folder and run *sphinx-apidoc -f -o source ../entero*. You
can also include any files or paths that you do not want included in the
documentation e.g.

.. code-block:: bash
    sphinx-apidoc -f -o source ../entero ../entero/databases/* ../entero/ExtraFuncs/unidecode

Next. run *make clean* and *make html*

Any top level python code will be run but the app will not have been
initiated leading to errors during the documentation creation. Therefore, you
can either exclude the module as above or put check to see if the app has
been initialized

.. code-block:: bash
    if app.config.get("PARAM_X"):
        x= app.config['PARAM_X"]
        ......

**manage.py**

The file containing scripts which run in the context of the app see
[flask-script](https://flask-script.readthedocs.io/en/latest/). The scripts
available are documented [here](Maintenance Scripts)

**requirements.txt**

A file containing all the python modules required. If extra modules are
added, this file should be updated by *pip freeze > requirements.txt*

**packages.txt**

Contains all the required linux packages