Updating Warwick Production Server
==================================
**These notes only apply to developers at University of Warwick.**

There are three production servers olkep,tomva and karmo. karmo is the main
front end running nginx and passes normal requests to either itself or tomva.
Requests for uploading reads and the api are sent to olkep Push your changes
to the master branch on bitbucket, the this needs to be propagated to the
three servers

**setup for code changes(hercules)**

* ssh to host using yoor university user name and private key
* cd /home/admin/enterobase-web
* sudo -u admin git pull
* sudo service enterobase restart

**setup for static files changes, e.g JavaScripts (karmo)**

* ssh to karmo using enterobase and the normal admistrative password (or key)
* cd /var/www/entero
* sudo -u enterobase git pull

**Update SSL certificate for NGINX**

* Admin needs to create KEY and CSR  file, they can create them using the following command:
 ::
 
   openssl req -new -newkey rsa:2048 -nodes -keyout enterobase.warwick.ac.uk.key -out enterobase.warwick.ac.uk.csr
* Admin can read “Generate a CSR” section in the following link to get more information about CSR generation process
 ::
 
	https://warwick.ac.uk/services/its/servicessupport/web/faqs/security_certificates 


* Then the admin can order an SSL certificate by filling the following form:
 ::
 
	https://warwick.service-now.com/sp?id=sc_cat_item&sys_id=1fa9728e37a41bc080198d2754990e1e
 
* Please note that the admin needs to upload CSR file when filling the form
* Usually, the admin, who submits the request, receives the SSL certificate by email within one working day.
* The certificate is a zip file which contains three files
 - CRT file, “enterobase_warwick_ac_uk.crt”
 - RootCertificates folder which contains two files: 
    - QuoVadisOVIntermediateCertificate.crt
    - QuoVadisOVRootCertificate.crt

* The admin needs to create a new file (enterobase.warwick.ac.uk.crt) which combines the contents of the previous three files, by copying and pasting each of them in the following order:

  - enterobase_warwick_ac_uk.crt
  - QuoVadisOVIntermediateCertificate.crt
  - QuoVadisOVRootCertificate.crt

* Then the admin needs to ssh to the vm which runs NGINX, i.e. karmo.lnx.warwick.ac.uk
* Download the two files and copy them to the following folder “/etc/nginx/ssl”
* The admin may use this command to test the configuration before restarting the server:
	 ::
	 
		sudo nginx -t

* If everything is fine, they may use the following command to reload the NGINX service 
	 ::
	 
	 sudo service nginx reload
