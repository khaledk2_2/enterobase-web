Documentation previously on mlst.warwick.ac.uk
==============================================
The website mlst.warwick.ac.uk for the old MLST databases at the University of 
Warwick is closed.

You can use the EnteroBase website as a replacement in parts for the old MLST 
website and much more. Information about MLST protocols previously found on 
the MLST site can be found here: 

.. toctree::
    :maxdepth: 1
    
    mlst-legacy-info-senterica
    mlst-legacy-info-ecoli
    mlst-legacy-info-yersinia
    mlst-legacy-info-mcatarrhalis