Downloading assemblies
======================
If a search has been carried out, assemblies can be downloaded by clicking in the 
boxes with the tick/ check icon heading (on the far left of the results table) 
in order to select the desired strains and then clicking on the icon - on the 
toolbar above the results table - with a blue circle with a white arrow inside 
in order to download the selected assemblies. (Assemblies will not be available 
for download in the case of entries for strains in the results from legacy MLST.)

An example including a search and download of assemblies is on the :doc:`searching-within-neighbours` page.
Programmers may also be interested in download of assemblies :doc:`api-download-schemes-assemblies <searching-within-neighbours>`.

  .. image:: https://bitbucket.org/repo/Xyayxn/images/862272432-6.png