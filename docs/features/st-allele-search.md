Searching Strains by ST or Allele
=================================

![search_sts.png](https://bitbucket.org/repo/Xyayxn/images/1808347651-search_sts.png)



Searching a strain with a similar ST or allelic profile enables you to find the nearest neighbors to your strain of interest. You can achieve this by clicking the link either in the top left hand panel on the database index page or the Find STs in the left hand menu. Clicking either of these will take you to the main search page and open a dialog box.  
![st_dialog.png](https://bitbucket.org/repo/Xyayxn/images/1721087311-st_dialog.png)


You can change the scheme using the dropdown. On small schemes you can search by allelic profile (2). Just enter the allele numbers and press submit. Any alleles left blank will be ignored i.e. will match with any allele

![st_context.png](https://bitbucket.org/repo/Xyayxn/images/611132344-st_context.png)

The query result will have the scheme used in the search chosen for Experimental Data and this will be displayed in the table on the right hand side.  In the case of ``Search On ST`` if ``Max Number MisMatches`` has a non-zero value then a Differences column will be included in the Experimental Data area and the results will be sorted in the order
of increasing differences.