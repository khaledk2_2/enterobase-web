#!/usr/bin/env python

import pytest
import os
from sys import stderr
from time import clock

from entero import create_app, db as database
from selenium import webdriver

from entero.databases.system.models import User

# Code borrowed from the "basic patterns and examples" page of
# the pytest documentation, in order to add the "--runslow"
# option when running pytest on the EnteroBase code.
def pytest_addoption(parser):
    parser.addoption("--runslow", action="store_true",
                     default=False, help="run slow tests")

def pytest_collection_modifyitems(config, items):
    if config.getoption("--runslow"):
        # --runslow given in cli: do not skip slow tests
        return
    skip_slow = pytest.mark.skip(reason="need --runslow option to run")
    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)

@pytest.fixture(scope='session')
def app():
    # app = create_app(config='test_settings')
    # app = create_app('development')
    # app = create_app(os.getenv('ENTERO_CONFIG'))
    # Martin says that "production" is the only currently vaild option
    # WVN 7/3/18 Except it looks like having a fully working "testing"
    # mode may help with opting out of loading up the admin stuff
    # which is suspected to cause problems.
    # Skipping the admin stuff seems to help but test runs with
    # and without "-n2" option in pytest suggest the underlying
    # problem is still there (suggesting some sort of problem with
    # a race condition and deadlock although we're not sure what gives
    # multiple threads/ processes in the case without "-n2" which actually
    # deadlocks).
    # In order to see time (or other) output from fixtures (or tests)
    # pytest must be run with "-s" option to dispable capturing.
    # print >> stderr, "**app fixture invoked**"
    start = clock()
 
    app = create_app('testing')
    #app = create_app('production')

    finish = clock()
    print >> stderr, "\nFIXTURE TIMING: app() took " + `(finish-start)` + " seconds"

    return app

@pytest.fixture(scope='session')
def eb_dbhandle(app):
    # app not explicitly used but this depends
    # on create_app() having been run which is done
    # by the app() method which has the fixture decoration
    start = clock()

    from entero import dbhandle
    eb_dbhandle = dbhandle

    finish = clock()
    print >> stderr, "\nFIXTURE TIMING: eb_dbhandle() took " + `(finish-start)` + " seconds"

    return eb_dbhandle

@pytest.fixture(scope='session')
def eb_client(app):
    start = clock()

    client = app.test_client(use_cookies = True)

    finish = clock()
    print >> stderr, "\nFIXTURE TIMING: eb_client() took " + `(finish-start)` + " seconds"

    return client

@pytest.fixture(scope='session')
def eb_logged_in_client(app):
    start = clock()

    client = app.test_client(use_cookies = True) 
    args={"email":"jack","password":"1234","remember_me":False}
    client.post("/auth/login",data=args)

    finish = clock()
    print >> stderr, "\nFIXTURE TIMING: eb_logged_in_client() took " + `(finish-start)` + " seconds"

    yield client
    # This is supposed to work like tear down
    client.get("/auth/logout")
    
@pytest.fixture(scope='session')
def eb_testboss_logged_in_client(app):
    start = clock()

    client = app.test_client(use_cookies = True) 
    args={"email":"testboss","password":"hUt3$tsw1nz","remember_me":False}
    client.post("/auth/login",data=args)

    finish = clock()
    print >> stderr, "\nFIXTURE TIMING: eb_testboss_logged_in_client() took " + `(finish-start)` + " seconds"

    yield client
    # This is supposed to work like tear down
    client.get("/auth/logout")

# API client does not need cookies so it's cleaner
# if it doesn't have them
@pytest.fixture(scope='session')
def eb_api_client(app):
    start = clock()

    client = app.test_client()

    finish = clock()
    print >> stderr, "\nFIXTURE TIMING: eb_api_client() took " + `(finish-start)` + " seconds"

    return client

#@pytest.fixture(scope='session')
#def eb_selenium_client(app):
#    options = webdriver.ChromeOptions()
#    options.add_argument("headless")
#    try:
#        ebDriver = webdriver.Chrome(chrome_options = options)
#    except: # I don't really like this - copied from book...
#        pass
#    return ebDriver

# WVN 28/2/18 Moved here from test_api.py and changed to session scope;
# because want to break up test_api.py into smaller files and the
# pytest documentation says __init__.py is not allowed in test directories.
# VTEMP - which makes things complicated in other ways - so temporarily
# commented this out.
@pytest.fixture(scope="session")
# WVN 14/2/18
# app is an argument since this depends on the app fixture.
# This looks weird; but it really does make whether or not
# you get an exception and a stack trace in the function below.
def eb_jack_api_key(app):
    # TEMP - later use the recommended approach
    # jackAPIKey = "eyJhbGciOiJIUzI1NiIsImV4cCI6MTU0OTk5MTExMywiaWF0IjoxNTE4NDU1MTEzfQ.eyJ1c2VybmFtZSI6ImphY2siLCJjaXR5IjoiTWV0cm9wb2xpcyIsImNvbmZpcm1lZCI6MSwiY2hhbmdlX2Fzc2VtYmx5X3N0YXR1cyI6IlRydWUiLCJmaXJzdG5hbWUiOiJKYWNrIiwiY2hhbmdlX3N0cmFpbl9vd25lciI6IlRydWUiLCJjaGFuZ2VfYXNzZW1ibHlfcmVsZWFzZV9kYXRlIjoiVHJ1ZSIsImNvdW50cnkiOiJVbml0ZWQgS2luZ2RvbSIsImlkIjoxNTgsImFkbWluaXN0cmF0b3IiOm51bGwsImFwaV9hY2Nlc3Nfc2VudGVyaWNhIjoiVHJ1ZSIsImVtYWlsIjoibS5qLnNlcmdlYW50MUBnb29nbGVtYWlsLmNvbSIsImRlcGFydG1lbnQiOiJXZWlyZCBDaGVtaWNhbHMgRGVwYXJ0bWVudCIsInZpZXdfc3BlY2llczEiOiJUcnVlIiwibGFzdG5hbWUiOiJTbWl0aCIsImFjdGl2ZSI6bnVsbCwidmlld19zcGVjaWVzIjoiVHJ1ZSIsImVkaXRfc3RyYWluX21ldGFkYXRhIjoiVHJ1ZSIsImRlbGV0ZV9zdHJhaW5zIjoiVHJ1ZSIsImluc3RpdHV0aW9uIjoiQUNNRSBsYWJzIiwidXBsb2FkX3JlYWRzMSI6IlRydWUifQ.p96LyX8s3-bKyhAoQw2fkkTc7jAeWFHuDLacQKLKfzE"
    # Borrowing code from species/views.py again since I don't
    # want to refresh the key which happens with the recommended
    # approach for users.
    start = clock()

    jackUser = User.query.filter_by(username='jack').first()
    jackAPIKey = jackUser.generate_api_token(expiration=31536000)

    finish = clock()
    print >> stderr, "\nFIXTURE TIMING: eb_jack_api_key() took " + `(finish-start)` + " seconds"

    return jackAPIKey
